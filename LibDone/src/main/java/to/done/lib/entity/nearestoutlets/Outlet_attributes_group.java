
package to.done.lib.entity.nearestoutlets;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "created_at",
    "modified_at",
    "name",
    "selection_max",
    "selection_min"
})
@DatabaseTable
public class Outlet_attributes_group {

    @JsonProperty("id")
    @DatabaseField(id = true)
	private Long id;
    @JsonProperty("created_at")
    @DatabaseField
	private String created_at;
    @JsonProperty("modified_at")
    @DatabaseField
	private String modified_at;
    @JsonProperty("name")
    @DatabaseField
	private String name;
    @JsonProperty("selection_max")
    @DatabaseField
	private Long selection_max=-1l;
    @JsonProperty("selection_min")
    @DatabaseField
	private Long selection_min=0l;

    @JsonIgnore
    private boolean selected;

    private List<Outlet_attribute>outletAttributeList= new ArrayList<Outlet_attribute>();

	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Long id) {
        this.id = id;
    }

    public Outlet_attributes_group withId(Long id) {
        this.id = id;
        return this;
    }

    @JsonProperty("created_at")
    public String getCreated_at() {
        return created_at;
    }

    @JsonProperty("created_at")
    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public Outlet_attributes_group withCreated_at(String created_at) {
        this.created_at = created_at;
        return this;
    }

    @JsonProperty("modified_at")
    public String getModified_at() {
        return modified_at;
    }

    @JsonProperty("modified_at")
    public void setModified_at(String modified_at) {
        this.modified_at = modified_at;
    }

    public Outlet_attributes_group withModified_at(String modified_at) {
        this.modified_at = modified_at;
        return this;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    public Outlet_attributes_group withName(String name) {
        this.name = name;
        return this;
    }

    @JsonProperty("selection_max")
    public Long getSelection_max() {
        return selection_max;
    }

    @JsonProperty("selection_max")
    public void setSelection_max(Long selection_max) {
        this.selection_max = selection_max;
    }

    public Outlet_attributes_group withSelection_max(Long selection_max) {
        this.selection_max = selection_max;
        return this;
    }

    @JsonProperty("selection_min")
    public Long getSelection_min() {
        return selection_min;
    }

    @JsonProperty("selection_min")
    public void setSelection_min(Long selection_min) {
        this.selection_min = selection_min;
    }

    public Outlet_attributes_group withSelection_min(Long selection_min) {
        this.selection_min = selection_min;
        return this;
    }



    	@Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public List<Outlet_attribute> getOutletAttributeList() {
        return outletAttributeList;
    }

    public void setOutletAttributeList(List<Outlet_attribute> outletAttributeList) {
        this.outletAttributeList = outletAttributeList;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
