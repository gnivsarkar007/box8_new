package to.done.lib.entity.order;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import to.done.lib.entity.user.User;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "user",
        "payment_method",
        "delivery_type",
        "is_preorder",
        "order_date",
        "total_amount",
        "orders",
        "client_platform",
        "client_platform_version",
        "client_version",
        "original_amt",
        "order_group_id",
        "order_number",
        "txn_id"
})

/**
 * Created by root on 27/10/14.
 */
public class DataForSaveCart {
    @JsonProperty("user")
    private User user;
    @JsonProperty("payment_method")
    private String paymentMethod;
    @JsonProperty("delivery_type")
    private String deliveryType;
    @JsonProperty("is_preorder")
    private Boolean isPreorder;
    @JsonProperty("order_date")
    private Long orderDate;
    @JsonProperty("total_amount")
    private Long totalAmount;
    @JsonProperty("orders")
    private List<Order> orders = new ArrayList<Order>();
    @JsonProperty("client_platform")
    private String clientPlatform;
    @JsonProperty("client_platform_version")
    private String clientPlatformVersion;
    @JsonProperty("client_version")
    private String clientVersion;
    @JsonProperty("original_amt")
    private Double originalAmt;
    @JsonProperty("order_group_id")
    private Long orderGroupId;
    @JsonProperty("order_number")
    private Long orderNumber;
    @JsonProperty("txn_id")
    private String txnId;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("user")
    public User getUser() {
        return user;
    }

    @JsonProperty("user")
    public void setUser(User user) {
        this.user = user;
    }

    @JsonProperty("payment_method")
    public String getPaymentMethod() {
        return paymentMethod;
    }

    @JsonProperty("payment_method")
    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    @JsonProperty("delivery_type")
    public String getDeliveryType() {
        return deliveryType;
    }

    @JsonProperty("delivery_type")
    public void setDeliveryType(String deliveryType) {
        this.deliveryType = deliveryType;
    }

    @JsonProperty("is_preorder")
    public Boolean getIsPreorder() {
        return isPreorder;
    }

    @JsonProperty("is_preorder")
    public void setIsPreorder(Boolean isPreorder) {
        this.isPreorder = isPreorder;
    }

    @JsonProperty("order_date")
    public Long getOrderDate() {
        return orderDate;
    }

    @JsonProperty("order_date")
    public void setOrderDate(Long orderDate) {
        this.orderDate = orderDate;
    }

    @JsonProperty("total_amount")
    public Long getTotalAmount() {
        return totalAmount;
    }

    @JsonProperty("total_amount")
    public void setTotalAmount(Long totalAmount) {
        this.totalAmount = totalAmount;
    }

    @JsonProperty("orders")
    public List<Order> getOrders() {
        return orders;
    }

    @JsonProperty("orders")
    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    @JsonProperty("client_platform")
    public String getClientPlatform() {
        return clientPlatform;
    }

    @JsonProperty("client_platform")
    public void setClientPlatform(String clientPlatform) {
        this.clientPlatform = clientPlatform;
    }

    @JsonProperty("client_platform_version")
    public String getClientPlatformVersion() {
        return clientPlatformVersion;
    }

    @JsonProperty("client_platform_version")
    public void setClientPlatformVersion(String clientPlatformVersion) {
        this.clientPlatformVersion = clientPlatformVersion;
    }

    @JsonProperty("client_version")
    public String getClientVersion() {
        return clientVersion;
    }

    @JsonProperty("client_version")
    public void setClientVersion(String clientVersion) {
        this.clientVersion = clientVersion;
    }

    @JsonProperty("original_amt")
    public Double getOriginalAmt() {
        return originalAmt;
    }

    @JsonProperty("original_amt")
    public void setOriginalAmt(Double originalAmt) {
        this.originalAmt = originalAmt;
    }

    @JsonProperty("order_group_id")
    public Long getOrderGroupId() {
        return orderGroupId;
    }

    @JsonProperty("order_group_id")
    public void setOrderGroupId(Long orderGroupId) {
        this.orderGroupId = orderGroupId;
    }

    @JsonProperty("order_number")
    public Long getOrderNumber() {
        return orderNumber;
    }

    @JsonProperty("order_number")
    public void setOrderNumber(Long orderNumber) {
        this.orderNumber = orderNumber;
    }

    @JsonProperty("txn_id")
    public String getTxnId() {
        return txnId;
    }

    @JsonProperty("txn_id")
    public void setTxnId(String txnId) {
        this.txnId = txnId;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}

