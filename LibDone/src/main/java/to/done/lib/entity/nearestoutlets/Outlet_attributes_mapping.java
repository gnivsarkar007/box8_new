
package to.done.lib.entity.nearestoutlets;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "outlet_id",
    "outlet_attributes_id",
    "value",
    "created_at",
    "modified_at",
    "outlet_attributes_mappingcol"
})
@DatabaseTable
public class Outlet_attributes_mapping {

    @JsonProperty("id")
    @DatabaseField(id = true)
    private Long id;
    @JsonProperty("outlet_id")
    @DatabaseField(index = true)
    private Long outlet_id;
    @JsonProperty("outlet_attributes_id")
    @DatabaseField(index = true)
    private Long outlet_attributes_id;
    @JsonProperty("value")
    @DatabaseField
    private String value;
    @JsonProperty("created_at")
    private String created_at;
    @JsonProperty("modified_at")
    private String modified_at;
    @JsonProperty("outlet_attributes_mappingcol")
    private Object outlet_attributes_mappingcol;

    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Long id) {
        this.id = id;
    }

    public Outlet_attributes_mapping withId(Long id) {
        this.id = id;
        return this;
    }

    @JsonProperty("outlet_id")
    public Long getOutlet_id() {
        return outlet_id;
    }

    @JsonProperty("outlet_id")
    public void setOutlet_id(Long outlet_id) {
        this.outlet_id = outlet_id;
    }

    public Outlet_attributes_mapping withOutlet_id(Long outlet_id) {
        this.outlet_id = outlet_id;
        return this;
    }

    @JsonProperty("outlet_attributes_id")
    public Long getOutlet_attributes_id() {
        return outlet_attributes_id;
    }

    @JsonProperty("outlet_attributes_id")
    public void setOutlet_attributes_id(Long outlet_attributes_id) {
        this.outlet_attributes_id = outlet_attributes_id;
    }

    public Outlet_attributes_mapping withOutlet_attributes_id(Long outlet_attributes_id) {
        this.outlet_attributes_id = outlet_attributes_id;
        return this;
    }

    @JsonProperty("value")
    public String getValue() {
        return value;
    }

    @JsonProperty("value")
    public void setValue(String value) {
        this.value = value;
    }

    public Outlet_attributes_mapping withValue(String value) {
        this.value = value;
        return this;
    }

    @JsonProperty("created_at")
    public String getCreated_at() {
        return created_at;
    }

    @JsonProperty("created_at")
    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public Outlet_attributes_mapping withCreated_at(String created_at) {
        this.created_at = created_at;
        return this;
    }

    @JsonProperty("modified_at")
    public String getModified_at() {
        return modified_at;
    }

    @JsonProperty("modified_at")
    public void setModified_at(String modified_at) {
        this.modified_at = modified_at;
    }

    public Outlet_attributes_mapping withModified_at(String modified_at) {
        this.modified_at = modified_at;
        return this;
    }

    @JsonProperty("outlet_attributes_mappingcol")
    public Object getOutlet_attributes_mappingcol() {
        return outlet_attributes_mappingcol;
    }

    @JsonProperty("outlet_attributes_mappingcol")
    public void setOutlet_attributes_mappingcol(Object outlet_attributes_mappingcol) {
        this.outlet_attributes_mappingcol = outlet_attributes_mappingcol;
    }

    public Outlet_attributes_mapping withOutlet_attributes_mappingcol(Object outlet_attributes_mappingcol) {
        this.outlet_attributes_mappingcol = outlet_attributes_mappingcol;
        return this;
    }



    	@Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
