package to.done.lib.entity.user;

/**
 * Created by root on 25/4/14.
 */

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;
@JsonInclude(JsonInclude.Include.NON_NULL)

@JsonPropertyOrder({
        "primary_phone"
})
public class RequestFindMe {

    @JsonProperty("primary_phone")
    private String primary_phone;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();


    @JsonProperty("primary_phone")
    public String getMobile_number() {
        return primary_phone;
    }

    @JsonProperty("primary_phone")
    public void setMobile_number(String mobile_number) {
        this.primary_phone = mobile_number;
    }



    	@Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}