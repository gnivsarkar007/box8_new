package to.done.lib.entity.tracking;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by HP on 6/25/2014.
 */
@JsonPropertyOrder({
        "order_time",
        "delivery_type",
        "total_amount",
        "order_id",
        "expected_delivery_time",
        "product_quantity",
        "order_status_list",
        "order_status_log"
})
public class TrackOrderData {

    @JsonProperty("order_time")
    private Long order_time;
    @JsonProperty("delivery_type")
    private String delivery_type;
    @JsonProperty("total_amount")
    private Long total_amount;
    @JsonProperty("order_id")
    private Long order_id;
    @JsonProperty("expected_delivery_time")
    private Long expected_delivery_time;
    @JsonProperty("product_quantity")
    private Long product_quantity;
    @JsonProperty("order_status_list")
    private List<Order_status_list> order_status_list = new ArrayList<Order_status_list>();
    @JsonProperty("order_status_log")
    private List<Order_status_log> order_status_log = new ArrayList<Order_status_log>();
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("order_time")
    public Long getOrder_time() {
        return order_time;
    }

    @JsonProperty("order_time")
    public void setOrder_time(Long order_time) {
        this.order_time = order_time;
    }

    @JsonProperty("delivery_type")
    public String getDelivery_type() {
        return delivery_type;
    }

    @JsonProperty("delivery_type")
    public void setDelivery_type(String delivery_type) {
        this.delivery_type = delivery_type;
    }

    @JsonProperty("total_amount")
    public Long getTotal_amount() {
        return total_amount;
    }

    @JsonProperty("total_amount")
    public void setTotal_amount(Long total_amount) {
        this.total_amount = total_amount;
    }

    @JsonProperty("order_id")
    public Long getOrder_id() {
        return order_id;
    }

    @JsonProperty("order_id")
    public void setOrder_id(Long order_id) {
        this.order_id = order_id;
    }

    @JsonProperty("expected_delivery_time")
    public Long getExpected_delivery_time() {
        return expected_delivery_time;
    }

    @JsonProperty("expected_delivery_time")
    public void setExpected_delivery_time(Long expected_delivery_time) {
        this.expected_delivery_time = expected_delivery_time;
    }

    @JsonProperty("product_quantity")
    public Long getProduct_quantity() {
        return product_quantity;
    }

    @JsonProperty("product_quantity")
    public void setProduct_quantity(Long product_quantity) {
        this.product_quantity = product_quantity;
    }

    @JsonProperty("order_status_list")
    public List<Order_status_list> getOrder_status_list() {
        return order_status_list;
    }

    @JsonProperty("order_status_list")
    public void setOrder_status_list(List<Order_status_list> order_status_list) {
        this.order_status_list = order_status_list;
    }

    @JsonProperty("order_status_log")
    public List<Order_status_log> getOrder_status_log() {
        return order_status_log;
    }

    @JsonProperty("order_status_log")
    public void setOrder_status_log(List<Order_status_log> order_status_log) {
        this.order_status_log = order_status_log;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return "TrackOrderData{" +
                "order_time=" + order_time +
                ", delivery_type='" + delivery_type + '\'' +
                ", total_amount=" + total_amount +
                ", order_id=" + order_id +
                ", expected_delivery_time=" + expected_delivery_time +
                ", product_quantity=" + product_quantity +
                ", order_status_list=" + order_status_list +
                ", order_status_log=" + order_status_log +
                '}';
    }
}