package to.done.lib.entity.payments;

/**
 * Created by root on 5/11/14.
 */

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "responseCode",
        "responseMsg",
        "data",
        "dataArray"
})
public class ResponseHMAC {

    @JsonProperty("responseCode")
    private Long responseCode;
    @JsonProperty("responseMsg")
    private String responseMsg;
    @JsonProperty("data")
    private String data;
    @JsonProperty("dataArray")
    private Object dataArray;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     * The responseCode
     */
    @JsonProperty("responseCode")
    public Long getResponseCode() {
        return responseCode;
    }

    /**
     *
     * @param responseCode
     * The responseCode
     */
    @JsonProperty("responseCode")
    public void setResponseCode(Long responseCode) {
        this.responseCode = responseCode;
    }

    /**
     *
     * @return
     * The responseMsg
     */
    @JsonProperty("responseMsg")
    public String getResponseMsg() {
        return responseMsg;
    }

    /**
     *
     * @param responseMsg
     * The responseMsg
     */
    @JsonProperty("responseMsg")
    public void setResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
    }

    /**
     *
     * @return
     * The data
     */
    @JsonProperty("data")
    public String getData() {
        return data;
    }

    /**
     *
     * @param data
     * The data
     */
    @JsonProperty("data")
    public void setData(String data) {
        this.data = data;
    }

    /**
     *
     * @return
     * The dataArray
     */
    @JsonProperty("dataArray")
    public Object getDataArray() {
        return dataArray;
    }

    /**
     *
     * @param dataArray
     * The dataArray
     */
    @JsonProperty("dataArray")
    public void setDataArray(Object dataArray) {
        this.dataArray = dataArray;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
