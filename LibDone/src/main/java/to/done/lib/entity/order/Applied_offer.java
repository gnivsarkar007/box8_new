
package to.done.lib.entity.order;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "coupon_code",
    "name",
    "description",
    "xml",
    "created_at",
    "modified_at",
    "offer_rules"
})
@DatabaseTable
public class Applied_offer implements Serializable {

    @JsonProperty("id")
   @DatabaseField(id=true)
    private Long id;
    @JsonProperty("coupon_code")
    @DatabaseField
    private String coupon_code;
    @JsonProperty("name")
    @DatabaseField
    private String name;
    @JsonProperty("description")
    @DatabaseField
    private String description;
    @JsonProperty("xml")
    @DatabaseField
    private String xml;
    @JsonProperty("created_at")
    @DatabaseField
    private String created_at;
    @JsonProperty("modified_at")
    @DatabaseField
    private String modified_at;
    @JsonProperty("offer_rules")
    private Offer_rules offer_rules;
    @JsonProperty("discount_amt")
    @DatabaseField
    private Double discount_amount;
    private transient Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Long id) {
        this.id = id;
    }

    @JsonProperty("discount_amt")
    public Double getDiscount_amount() {
        return discount_amount;
    }

    @JsonProperty("discount_amt")
    public void setDiscount_amount(Double discount_amount) {
        this.discount_amount = discount_amount;
    }

    public Applied_offer withId(Long id) {
        this.id = id;
        return this;
    }

    @JsonProperty("coupon_code")
    public String getCoupon_code() {
        return coupon_code;
    }

    @JsonProperty("coupon_code")
    public void setCoupon_code(String coupon_code) {
        this.coupon_code = coupon_code;
    }

    public Applied_offer withCoupon_code(String coupon_code) {
        this.coupon_code = coupon_code;
        return this;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    public Applied_offer withName(String name) {
        this.name = name;
        return this;
    }

    @JsonProperty("description")
    public Object getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    public Applied_offer withDescription(String description) {
        this.description = description;
        return this;
    }

    @JsonProperty("xml")
    public Object getXml() {
        return xml;
    }

    @JsonProperty("xml")
    public void setXml(String xml) {
        this.xml = xml;
    }

    public Applied_offer withXml(String xml) {
        this.xml = xml;
        return this;
    }

    @JsonProperty("created_at")
    public String getCreated_at() {
        return created_at;
    }

    @JsonProperty("created_at")
    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public Applied_offer withCreated_at(String created_at) {
        this.created_at = created_at;
        return this;
    }

    @JsonProperty("modified_at")
    public String getModified_at() {
        return modified_at;
    }

    @JsonProperty("modified_at")
    public void setModified_at(String modified_at) {
        this.modified_at = modified_at;
    }

    public Applied_offer withModified_at(String modified_at) {
        this.modified_at = modified_at;
        return this;
    }

    @JsonProperty("offer_rules")
    public Offer_rules getOffer_rules() {
        return offer_rules;
    }

    @JsonProperty("offer_rules")
    public void setOffer_rules(Offer_rules offer_rules) {
        this.offer_rules = offer_rules;
    }

    public Applied_offer withOffer_rules(Offer_rules offer_rules) {
        this.offer_rules = offer_rules;
        return this;
    }



    	@Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
