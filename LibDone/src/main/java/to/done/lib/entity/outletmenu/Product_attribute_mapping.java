
package to.done.lib.entity.outletmenu;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "product_id",
    "product_attribute_id",
    "value",
    "row_status",
    "created_at",
    "modified_at"
})
@DatabaseTable
public class Product_attribute_mapping {

    @JsonProperty("id")
    @DatabaseField(id = true)
	private Long id;
    @JsonProperty("product_id")
    @DatabaseField(index = true)
	private Long product_id;
    @JsonProperty("product_attribute_id")
    @DatabaseField(index = true)
	private Long product_attribute_id;
    @JsonProperty("value")
    @DatabaseField
	private String value;
    @JsonProperty("row_status")
    @DatabaseField
	private String row_status;
    @JsonProperty("created_at")
    @DatabaseField
	private String created_at;
    @JsonProperty("modified_at")
    @DatabaseField
	private String modified_at;

    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Long id) {
        this.id = id;
    }

    public Product_attribute_mapping withId(Long id) {
        this.id = id;
        return this;
    }

    @JsonProperty("product_id")
    public Long getProduct_id() {
        return product_id;
    }

    @JsonProperty("product_id")
    public void setProduct_id(Long product_id) {
        this.product_id = product_id;
    }

    public Product_attribute_mapping withProduct_id(Long product_id) {
        this.product_id = product_id;
        return this;
    }

    @JsonProperty("product_attribute_id")
    public Long getProduct_attribute_id() {
        return product_attribute_id;
    }

    @JsonProperty("product_attribute_id")
    public void setProduct_attribute_id(Long product_attribute_id) {
        this.product_attribute_id = product_attribute_id;
    }

    public Product_attribute_mapping withProduct_attribute_id(Long product_attribute_id) {
        this.product_attribute_id = product_attribute_id;
        return this;
    }

    @JsonProperty("value")
    public String getValue() {
        return value;
    }

    @JsonProperty("value")
    public void setValue(String value) {
        this.value = value;
    }

    public Product_attribute_mapping withValue(String value) {
        this.value = value;
        return this;
    }

    @JsonProperty("row_status")
    public String getRow_status() {
        return row_status;
    }

    @JsonProperty("row_status")
    public void setRow_status(String row_status) {
        this.row_status = row_status;
    }

    public Product_attribute_mapping withRow_status(String row_status) {
        this.row_status = row_status;
        return this;
    }

    @JsonProperty("created_at")
    public String getCreated_at() {
        return created_at;
    }

    @JsonProperty("created_at")
    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public Product_attribute_mapping withCreated_at(String created_at) {
        this.created_at = created_at;
        return this;
    }

    @JsonProperty("modified_at")
    public String getModified_at() {
        return modified_at;
    }

    @JsonProperty("modified_at")
    public void setModified_at(String modified_at) {
        this.modified_at = modified_at;
    }

    public Product_attribute_mapping withModified_at(String modified_at) {
        this.modified_at = modified_at;
        return this;
    }



    	@Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
