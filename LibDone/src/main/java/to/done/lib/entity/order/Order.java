package to.done.lib.entity.order;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import to.done.lib.entity.outletmenu.Product;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "company_id",
        "outlet_id",
        "coupon_code",
        "total",
        "comments",
        "products",
        "order_id"
})
@DatabaseTable
public class Order implements Serializable {



    @DatabaseField
    @JsonProperty("company_id")
    private Long company_id;
    @DatabaseField
    @JsonProperty("outlet_id")
    private Long outlet_id;
    @DatabaseField
    @JsonProperty("coupon_code")
    private String coupon_code="";
    @DatabaseField
    @JsonProperty("total")
    private Double total=0d;
    @DatabaseField
    @JsonProperty("comments")
    private String comments="";
    @JsonProperty("products")
    private List<Product> products = new ArrayList<Product>();
    private transient Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @DatabaseField(id=true)
    @JsonProperty("order_id")
    Long order_id;

    @JsonProperty("order_id")
    public Long getOrder_id() {
        return order_id;
    }
    @JsonProperty("order_id")
    public void setOrder_id(Long order_id) {
        this.order_id = order_id;
    }

    @JsonIgnore
    @DatabaseField
    Long order_date;

    @JsonIgnore
    @DatabaseField
    Integer quantity;
    @DatabaseField
    @JsonProperty("order_number")
    Integer order_number;
    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getOrder_number() {
        return order_number;
    }

    public void setOrder_number(Integer order_number) {
        this.order_number = order_number;
    }

    @JsonProperty("applied_extra_charges")
    private List<Applied_extra_charge> applied_extra_charges = new ArrayList<Applied_extra_charge>();


    @JsonProperty("applied_offer")
    private List<Applied_offer> applied_offer = new ArrayList<Applied_offer>();
    @JsonProperty("applied_extra_charges")
    public List<Applied_extra_charge> getApplied_extra_charges() {
        return applied_extra_charges;
    }

    @JsonProperty("applied_extra_charges")
    public void setApplied_extra_charges(List<Applied_extra_charge> applied_extra_charges) {
        this.applied_extra_charges = applied_extra_charges;
    }
    @JsonProperty("applied_offer")
    public List<Applied_offer> getApplied_offer() {
        return applied_offer;
    }
    @JsonProperty("applied_offer")
    public void setApplied_offer(List<Applied_offer> applied_offer) {
        this.applied_offer = applied_offer;
    }

    public Long getOrder_date() {
        return order_date;
    }

    public void setOrder_date(Long order_date) {
        this.order_date = order_date;
    }

    public Order() {
    }

    public Order(Long company_id, Long outlet_id) {
        this.company_id = company_id;
        this.outlet_id = outlet_id;
    }

    @JsonProperty("company_id")
    public Long getCompany_id() {
        return company_id;
    }

    @JsonProperty("company_id")
    public void setCompany_id(Long company_id) {
        this.company_id = company_id;
    }

    @JsonProperty("outlet_id")
    public Long getOutlet_id() {
        return outlet_id;
    }

    @JsonProperty("outlet_id")
    public void setOutlet_id(Long outlet_id) {
        this.outlet_id = outlet_id;
    }

    @JsonProperty("coupon_code")
    public String getCoupon_code() {
        return coupon_code;
    }

    @JsonProperty("coupon_code")
    public void setCoupon_code(String coupon_code) {
        this.coupon_code = coupon_code;
    }

    @JsonProperty("total")
    public Double getTotal() {
        return total;
    }

    @JsonProperty("total")
    public void setTotal(Double total) {
        this.total = total;
    }

    @JsonProperty("comments")
    public String getComments() {
        return comments;
    }

    @JsonProperty("comments")
    public void setComments(String comments) {
        this.comments = comments;
    }

    @JsonProperty("products")
    public List<Product> getProducts() {
        return products;
    }

    @JsonProperty("products")
    public void setProducts(List<Product> products) {
        this.products = products;
    }



    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}