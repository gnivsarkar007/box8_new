package to.done.lib.entity.order;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)

@JsonPropertyOrder({
        "is_preorder",
        "order_date",
        "coupon_code",
        "total_amount",
        "delivery_type",
        "orders"
})
public class RequestCheckOrder {

    @JsonProperty("is_preorder")
    private Boolean is_preorder;
    @JsonProperty("order_date")
    private Long order_date;
    @JsonProperty("coupon_code")
    private String coupon_code;
    @JsonProperty("total_amount")
    private Double total_amount;
    @JsonProperty("delivery_type")
    String delivery_type;
    @JsonProperty("orders")
    private Collection<Order> orders = new ArrayList<Order>();
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public RequestCheckOrder(Boolean is_preorder, Long order_date,String coupon_code, Double total_amount,String delivery_type ,Collection<Order> orders) {
        this.is_preorder = is_preorder;
        this.order_date = order_date;
        this.coupon_code = coupon_code;
        this.total_amount = total_amount;
        this.delivery_type=delivery_type;
        this.orders = orders;
    }
    @JsonProperty("delivery_type")
    public String getDelivery_type() {
        return delivery_type;
    }

    @JsonProperty("delivery_type")
    public void setDelivery_type(String delivery_type) {
        this.delivery_type = delivery_type;
    }

    @JsonProperty("is_preorder")
    public Boolean getIs_preorder() {
        return is_preorder;
    }

    @JsonProperty("is_preorder")
    public void setIs_preorder(Boolean is_preorder) {
        this.is_preorder = is_preorder;
    }

    @JsonProperty("order_date")
    public Long getOrder_date() {
        return order_date;
    }

    @JsonProperty("order_date")
    public void setOrder_date(Long order_date) {
        this.order_date = order_date;
    }

    @JsonProperty("coupon_code")
    public String getCoupon_code() {
        return coupon_code;
    }

    @JsonProperty("coupon_code")
    public void setCoupon_code(String coupon_code) {
        this.coupon_code = coupon_code;
    }

    @JsonProperty("total_amount")
    public Double getTotal_amount() {
        return total_amount;
    }

    @JsonProperty("total_amount")
    public void setTotal_amount(Double total_amount) {
        this.total_amount = total_amount;
    }

    @JsonProperty("orders")
    public Collection<Order> getOrders() {
        return orders;
    }

    @JsonProperty("orders")
    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }



    	@Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}

