package to.done.lib.entity.cachingtables;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by root on 25/4/14.
 */
@DatabaseTable
public class CacheForData {
    @DatabaseField(generatedId=true)
    Integer id;
    @DatabaseField
    String name;
    @DatabaseField
    Long timestamp;
    @DatabaseField
    Long outlet_id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public Long getOutlet_id() {
        return outlet_id;
    }

    public void setOutlet_id(Long outlet_id) {
        this.outlet_id = outlet_id;
    }

    public static CacheForData getCacheObject(String name,Long timeStamp,Long outletId){
        CacheForData data= new CacheForData();
        data.setOutlet_id(outletId);
        data.setTimestamp(timeStamp);
        data.setName(name);
        return data;

    }

    @Override
    public String toString() {
        return "CacheForData{" +
                "name='" + name + '\'' +
                ", timestamp=" + timestamp +
                ", outlet_id=" + outlet_id +
                '}';
    }
}