
package to.done.lib.entity.nearestoutlets;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "created_at",
    "modified_at",
    "open_time",
    "duration",
    "day_of_week",
    "outlet_id",
    "row_status"
})
@DatabaseTable
public class Outlet_timing {

    @JsonProperty("id")
    @DatabaseField(id = true)
	private Long id;
    @JsonProperty("created_at")
    @DatabaseField
	private String created_at;
    @JsonProperty("modified_at")
    @DatabaseField
	private String modified_at;
    @JsonProperty("open_time")
    @DatabaseField
	private String open_time;
    @JsonProperty("duration")
    @DatabaseField
	private String duration;
    @JsonProperty("day_of_week")
    @DatabaseField
	private String day_of_week;
    @JsonProperty("outlet_id")
    @DatabaseField
	private Long outlet_id;
    @JsonProperty("row_status")
    @DatabaseField
	private String row_status;

    @JsonIgnore
    private String close_time;

	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Long id) {
        this.id = id;
    }

    public Outlet_timing withId(Long id) {
        this.id = id;
        return this;
    }

    @JsonProperty("created_at")
    public String getCreated_at() {
        return created_at;
    }

    @JsonProperty("created_at")
    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public Outlet_timing withCreated_at(String created_at) {
        this.created_at = created_at;
        return this;
    }

    @JsonProperty("modified_at")
    public String getModified_at() {
        return modified_at;
    }

    @JsonProperty("modified_at")
    public void setModified_at(String modified_at) {
        this.modified_at = modified_at;
    }

    public Outlet_timing withModified_at(String modified_at) {
        this.modified_at = modified_at;
        return this;
    }

    @JsonProperty("open_time")
    public String getOpen_time() {
        return open_time;
    }

    @JsonProperty("open_time")
    public void setOpen_time(String open_time) {
        this.open_time = open_time;
    }

    public Outlet_timing withOpen_time(String open_time) {
        this.open_time = open_time;
        return this;
    }

    @JsonProperty("duration")
    public String getDuration() {
        return duration;
    }

    @JsonProperty("duration")
    public void setDuration(String duration) {
        this.duration = duration;
    }

    public Outlet_timing withDuration(String duration) {
        this.duration = duration;
        return this;
    }

    @JsonProperty("day_of_week")
    public String getDay_of_week() {
        return day_of_week;
    }

    @JsonProperty("day_of_week")
    public void setDay_of_week(String day_of_week) {
        this.day_of_week = day_of_week;
    }

    public Outlet_timing withDay_of_week(String day_of_week) {
        this.day_of_week = day_of_week;
        return this;
    }

    @JsonProperty("outlet_id")
    public Long getOutlet_id() {
        return outlet_id;
    }

    @JsonProperty("outlet_id")
    public void setOutlet_id(Long outlet_id) {
        this.outlet_id = outlet_id;
    }

    public Outlet_timing withOutlet_id(Long outlet_id) {
        this.outlet_id = outlet_id;
        return this;
    }

    @JsonProperty("row_status")
    public String getRow_status() {
        return row_status;
    }

    @JsonProperty("row_status")
    public void setRow_status(String row_status) {
        this.row_status = row_status;
    }

    public Outlet_timing withRow_status(String row_status) {
        this.row_status = row_status;
        return this;
    }

    @JsonIgnore
    public String getClose_time() {
        return close_time;
    }

    @JsonIgnore
    public void setClose_time(String close_time) {
        this.close_time = close_time;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
