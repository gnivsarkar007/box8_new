package to.done.lib.entity.user;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by root on 27/6/14.
 */
@JsonPropertyOrder({
        "id",
        "primary_phone",
        "primary_email",
        "name",
        "facebook_id",
        "google_id",
        "created_at",
        "modified_at",
        "addresses"
})
public class FindMeData {

    @JsonProperty("id")
    private Long id;
    @JsonProperty("primary_phone")
    private String primary_phone;
    @JsonProperty("primary_email")
    private String primary_email;
    @JsonProperty("name")
    private String name;
    @JsonProperty("facebook_id")
    private String facebook_id;
    @JsonProperty("google_id")
    private String google_id;
    @JsonProperty("created_at")
    private String created_at;
    @JsonProperty("modified_at")
    private String modified_at;
    @JsonProperty("addresses")
    private List<Address> addresses = new ArrayList<Address>();
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    @JsonIgnore
    private User user=new User();

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Long id) {
        this.id = id;
        user.setId(getId());
    }

    @JsonProperty("primary_phone")
    public String getPrimary_phone() {
        return primary_phone;
    }

    @JsonProperty("primary_phone")
    public void setPrimary_phone(String primary_phone) {
        this.primary_phone = primary_phone;
        user.setPrimary_phone(primary_phone);
    }

    @JsonProperty("primary_email")
    public String getPrimary_email() {
        return primary_email;
    }

    @JsonProperty("primary_email")
    public void setPrimary_email(String primary_email) {
        this.primary_email = primary_email;
        user.setPrimary_email(primary_email);
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
        user.setName(name);
    }

    @JsonProperty("facebook_id")
    public String getFacebook_id() {
        return facebook_id;
    }

    @JsonProperty("facebook_id")
    public void setFacebook_id(String facebook_id) {
        this.facebook_id = facebook_id;

    }

    @JsonProperty("google_id")
    public String getGoogle_id() {
        return google_id;
    }

    @JsonProperty("google_id")
    public void setGoogle_id(String google_id) {
        this.google_id = google_id;
    }

    @JsonProperty("created_at")
    public String getCreated_at() {
        return created_at;
    }

    @JsonProperty("created_at")
    public void setCreated_at(String created_at) {
        this.created_at = created_at;

    }

    @JsonProperty("modified_at")
    public String getModified_at() {
        return modified_at;
    }

    @JsonProperty("modified_at")
    public void setModified_at(String modified_at) {
        this.modified_at = modified_at;
    }

    @JsonProperty("addresses")
    public List<Address> getAddresses() {
        return addresses;
    }

    @JsonProperty("addresses")
    public void setAddresses(List<Address> addresses) {
        this.addresses = addresses;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
