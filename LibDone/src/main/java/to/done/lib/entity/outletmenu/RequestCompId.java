package to.done.lib.entity.outletmenu;

/**
 * Created by HP on 5/16/2014.
 */

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({
        "company_id",
        "outlet_last_updated_timestamp",
        "ot_last_updated_timestamp",
        "oa_last_updated_timestamp",
        "oam_last_updated_timestamp",
        "oag_last_updated_timestamp",
        "offer_last_updated_timestamp",
        "oom_last_updated_timestamp"
})
public class RequestCompId {
    private Long company_id;
    @JsonProperty("outlet_last_updated_timestamp")
    private Long outlet_last_updated_timestamp;



    public RequestCompId(Long company_id) {
        this.company_id = company_id;
        this.outlet_last_updated_timestamp = 0l;
        this.ot_last_updated_timestamp = 0l;
        this.oa_last_updated_timestamp = 0l;
        this.oam_last_updated_timestamp = 0l;
        this.oag_last_updated_timestamp = 0l;
        this.offer_last_updated_timestamp = 0l;
        this.oom_last_updated_timestamp = 0l;

    }

    @JsonProperty("ot_last_updated_timestamp")
    private Long ot_last_updated_timestamp;
    @JsonProperty("oa_last_updated_timestamp")
    private Long oa_last_updated_timestamp;
    @JsonProperty("oam_last_updated_timestamp")
    private Long oam_last_updated_timestamp;
    @JsonProperty("oag_last_updated_timestamp")
    private Long oag_last_updated_timestamp;
    @JsonProperty("offer_last_updated_timestamp")
    private Long offer_last_updated_timestamp;
    @JsonProperty("oom_last_updated_timestamp")
    private Long oom_last_updated_timestamp;

    public Long getOutlet_last_updated_timestamp() {
        return outlet_last_updated_timestamp;
    }

    public void setOutlet_last_updated_timestamp(Long outlet_last_updated_timestamp) {
        this.outlet_last_updated_timestamp = outlet_last_updated_timestamp;
    }

    public Long getOt_last_updated_timestamp() {
        return ot_last_updated_timestamp;
    }

    public void setOt_last_updated_timestamp(Long ot_last_updated_timestamp) {
        this.ot_last_updated_timestamp = ot_last_updated_timestamp;
    }

    public Long getOa_last_updated_timestamp() {
        return oa_last_updated_timestamp;
    }

    public void setOa_last_updated_timestamp(Long oa_last_updated_timestamp) {
        this.oa_last_updated_timestamp = oa_last_updated_timestamp;
    }

    public Long getOam_last_updated_timestamp() {
        return oam_last_updated_timestamp;
    }

    public void setOam_last_updated_timestamp(Long oam_last_updated_timestamp) {
        this.oam_last_updated_timestamp = oam_last_updated_timestamp;
    }

    public Long getOag_last_updated_timestamp() {
        return oag_last_updated_timestamp;
    }

    public void setOag_last_updated_timestamp(Long oag_last_updated_timestamp) {
        this.oag_last_updated_timestamp = oag_last_updated_timestamp;
    }

    public Long getOffer_last_updated_timestamp() {
        return offer_last_updated_timestamp;
    }

    public void setOffer_last_updated_timestamp(Long offer_last_updated_timestamp) {
        this.offer_last_updated_timestamp = offer_last_updated_timestamp;
    }

    public Long getOom_last_updated_timestamp() {
        return oom_last_updated_timestamp;
    }

    public void setOom_last_updated_timestamp(Long oom_last_updated_timestamp) {
        this.oom_last_updated_timestamp = oom_last_updated_timestamp;
    }

    public Long getCompany_id() {
        return company_id;
    }

    public void setCompany_id(Long company_id) {
        this.company_id = company_id;
    }

    @Override
    public String toString() {
        return "RequestCompId{" +
                "company_id=" + company_id +
                '}';
    }
}
