package to.done.lib.entity.nearestoutlets;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"subarea_id",
"service_type"
})
public class RequestNearestOutlets {

@JsonProperty("subarea_id")
private Long subarea_id;
@JsonProperty("service_type")
private String service_type;
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

@JsonProperty("subarea_id")
public Long getSubarea_id() {
return subarea_id;
}

@JsonProperty("subarea_id")
public void setSubarea_id(Long subarea_id) {
this.subarea_id = subarea_id;
}

    public RequestNearestOutlets(Long subarea_id, String service_type) {
        this.subarea_id = subarea_id;
        this.service_type = service_type;
    }

    public RequestNearestOutlets withSubarea_id(Long subarea_id) {
this.subarea_id = subarea_id;
return this;
}

@JsonProperty("service_type")
public String getService_type() {
return service_type;
}

@JsonProperty("service_type")
public void setService_type(String service_type) {
this.service_type = service_type;
}

public RequestNearestOutlets withService_type(String service_type) {
this.service_type = service_type;
return this;
}

@Override
public String toString() {
return ToStringBuilder.reflectionToString(this);
}

@Override
public int hashCode() {
return HashCodeBuilder.reflectionHashCode(this);
}

@Override
public boolean equals(Object other) {
return EqualsBuilder.reflectionEquals(this, other);
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}