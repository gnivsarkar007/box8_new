
package to.done.lib.entity.outletmenu;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "responseCode",
    "responseMsg",
    "data",
    "dataArray"
})
public class RespOutletMenu {

    @JsonProperty("responseCode")
    private Long responseCode;
    @JsonProperty("responseMsg")
    private String responseMsg;
    @JsonProperty("data")
    private OutletMenuData data;
    @JsonProperty("dataArray")
    private List<OutletMenuData> dataArray = new ArrayList<OutletMenuData>();
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("responseCode")
    public Long getResponseCode() {
        return responseCode;
    }

    @JsonProperty("responseCode")
    public void setResponseCode(Long responseCode) {
        this.responseCode = responseCode;
    }

    public RespOutletMenu withResponseCode(Long responseCode) {
        this.responseCode = responseCode;
        return this;
    }

    @JsonProperty("responseMsg")
    public String getResponseMsg() {
        return responseMsg;
    }

    @JsonProperty("responseMsg")
    public void setResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
    }

    public RespOutletMenu withResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
        return this;
    }

    @JsonProperty("data")
    public OutletMenuData getData() {
        return data;
    }

    @JsonProperty("data")
    public void setData(OutletMenuData data) {
        this.data = data;
    }

    public RespOutletMenu withData(OutletMenuData data) {
        this.data = data;
        return this;
    }

    @JsonProperty("dataArray")
    public List<OutletMenuData> getDataArray() {
        return dataArray;
    }

    @JsonProperty("dataArray")
    public void setDataArray(List<OutletMenuData> dataArray) {
        this.dataArray = dataArray;
    }

    public RespOutletMenu withDataArray(List<OutletMenuData> dataArray) {
        this.dataArray = dataArray;
        return this;
    }



    	@Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
