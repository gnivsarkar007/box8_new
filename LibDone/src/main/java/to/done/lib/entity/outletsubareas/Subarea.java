package to.done.lib.entity.outletsubareas;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by HP on 6/15/2014.
 */
@JsonPropertyOrder({
        "id",
        "subarea_name",
        "area_id"
})
@DatabaseTable
public class Subarea {

    @JsonProperty("id")
    @DatabaseField(id=true)
    private Long id;
    @JsonProperty("subarea_name")
    @DatabaseField
    private String subarea_name;

    @JsonProperty("area_id")
    @DatabaseField
    private Long area_id;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @DatabaseField
    private Long outlet_id;

    public Long getOutlet_id() {
        return outlet_id;
    }

    public void setOutlet_id(Long outlet_id) {
        this.outlet_id = outlet_id;
    }

    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Long id) {
        this.id = id;
    }

    @JsonProperty("subarea_name")
    public String getSubarea_name() {
        return subarea_name;
    }

    @JsonProperty("subarea_name")
    public void setSubarea_name(String subarea_name) {
        this.subarea_name = subarea_name;
    }

    @JsonProperty("area_id")
    public Long getArea_id() {
        return area_id;
    }

    @JsonProperty("area_id")
    public void setArea_id(Long area_id) {
        this.area_id = area_id;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Subarea)) return false;

        Subarea subarea = (Subarea) o;

        if (!id.equals(subarea.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public String toString() {
        return subarea_name;
    }
}
