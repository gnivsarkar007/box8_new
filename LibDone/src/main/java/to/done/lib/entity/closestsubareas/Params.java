
package to.done.lib.entity.closestsubareas;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "fl",
    "sort",
    "indent",
    "start",
    "q",
    "sfield",
    "pt",
    "wt",
    "rows"
})
public class Params {
    @JsonProperty("sort")
    private String sort;
    @JsonProperty("indent")
    private String indent;
    @JsonProperty("start")
    private String start;
    @JsonProperty("q")
    private String q;
    @JsonProperty("sfield")
    private String sfield;
    @JsonProperty("pt")
    private String pt;
    @JsonProperty("wt")
    private String wt;
    @JsonProperty("rows")
    private String rows;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();


    @JsonProperty("sort")
    public String getSort() {
        return sort;
    }

    @JsonProperty("sort")
    public void setSort(String sort) {
        this.sort = sort;
    }

    public Params withSort(String sort) {
        this.sort = sort;
        return this;
    }

    @JsonProperty("indent")
    public String getIndent() {
        return indent;
    }

    @JsonProperty("indent")
    public void setIndent(String indent) {
        this.indent = indent;
    }

    public Params withIndent(String indent) {
        this.indent = indent;
        return this;
    }

    @JsonProperty("start")
    public String getStart() {
        return start;
    }

    @JsonProperty("start")
    public void setStart(String start) {
        this.start = start;
    }

    public Params withStart(String start) {
        this.start = start;
        return this;
    }

    @JsonProperty("q")
    public String getQ() {
        return q;
    }

    @JsonProperty("q")
    public void setQ(String q) {
        this.q = q;
    }

    public Params withQ(String q) {
        this.q = q;
        return this;
    }

    @JsonProperty("sfield")
    public String getSfield() {
        return sfield;
    }

    @JsonProperty("sfield")
    public void setSfield(String sfield) {
        this.sfield = sfield;
    }

    public Params withSfield(String sfield) {
        this.sfield = sfield;
        return this;
    }

    @JsonProperty("pt")
    public String getPt() {
        return pt;
    }

    @JsonProperty("pt")
    public void setPt(String pt) {
        this.pt = pt;
    }

    public Params withPt(String pt) {
        this.pt = pt;
        return this;
    }

    @JsonProperty("wt")
    public String getWt() {
        return wt;
    }

    @JsonProperty("wt")
    public void setWt(String wt) {
        this.wt = wt;
    }

    public Params withWt(String wt) {
        this.wt = wt;
        return this;
    }

    @JsonProperty("rows")
    public String getRows() {
        return rows;
    }

    @JsonProperty("rows")
    public void setRows(String rows) {
        this.rows = rows;
    }

    public Params withRows(String rows) {
        this.rows = rows;
        return this;
    }



    	@Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
