package to.done.lib.entity.outletsubareas;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by HP on 6/15/2014.
 */
    @JsonPropertyOrder({
            "outlet_id"
    })
    public class RequestOutletSubarea {

        @JsonProperty("outlet_id")
        private Long outlet_id;
        private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public RequestOutletSubarea(long outletID){
        this.outlet_id=outletID;
    }
        @JsonProperty("outlet_id")
        public Long getOutlet_id() {
            return outlet_id;
        }

        @JsonProperty("outlet_id")
        public void setOutlet_id(Long outlet_id) {
            this.outlet_id = outlet_id;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }

    }
