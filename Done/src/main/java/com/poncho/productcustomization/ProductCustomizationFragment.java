package com.poncho.productcustomization;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.poncho.MainActivity;
import com.poncho.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import to.done.lib.Constants;
import to.done.lib.cart.Cart;
import to.done.lib.database.DBManager;
import to.done.lib.entity.order.Customization;
import to.done.lib.entity.outletmenu.Product;
import to.done.lib.utils.Toolbox;

/**
 * Created by dipenpradhan on 4/24/14.
 */
public class ProductCustomizationFragment extends Fragment{

    @InjectView(R.id.ex_lst_product_customization)
    ExpandableListView exLstProductCustomization;

    @InjectView(R.id.txt_add)
    TextView txtAdd;


    EditText edtCommentsBox;

    @InjectView(R.id.txt_cancel)
    TextView txtCancel;

    @InjectView(R.id.txt_quantity_val)
    TextView txtQuantityVal;


    @InjectView(R.id.img_minus)
    ImageView imgMinus;

    @InjectView(R.id.img_plus)
    ImageView imgPlus;

    private long outletProductId;

    private DBManager dbMan;
    private Cart cart= Cart.getInstance();
    View footerview=null;
    String custsize;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.product_customization,null);

        ButterKnife.inject(this,view);
        init();
        return view;
    }

private void init(){
    ((MainActivity)getActivity()).setCurrentScreenId(Constants.SCREEN_PRODUCT_CUSTOMIZATION);
    ((MainActivity)getActivity()).drawerOpen=false;
    dbMan=DBManager.getInstance(getActivity().getApplication());

    final long productId=getArguments().getLong(Constants.PRODUCT_ID);
    String productName=getArguments().getString(Constants.PRODUCT_NAME);
    final long outletId=getArguments().getLong(Constants.OUTLET_ID);
    final long companyId=getArguments().getLong(Constants.COMPANY_ID);
    custsize= getArguments().getString(Constants.CUST_SIZE);
    ImageView img=(ImageView)((ActionBarActivity)getActivity()).getSupportActionBar().getCustomView().findViewById(R.id.imgBack);
    img.setVisibility(View.VISIBLE);
    outletProductId=getArguments().getLong(Constants.OUTLET_PRODUCT_ID);

    txtAdd.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            getActivity().onBackPressed();
        }
    });

    if(custsize.toLowerCase().equals("yes")) {
        final Product product= dbMan.getProductByOutletProductId(outletProductId);
        Toolbox.changeActionBarTitle(getActivity(),"Customize Your Order",product.getName());
        final List<Customization> customizationList=new ArrayList<Customization>();

        exLstProductCustomization.setAdapter(new ProductCustomizationExpListAdapter(getActivity(), outletProductId));
        addFooterView();
        int groupCount = exLstProductCustomization.getExpandableListAdapter().getGroupCount();
        int count = groupCount;

        for (int i = 0; i < groupCount; i++) {
            count += exLstProductCustomization.getExpandableListAdapter().getChildrenCount(i);
        }


//        if(count<15){
        for (int i = 0; i < groupCount; i++) {
            exLstProductCustomization.expandGroup(i);
        }
        exLstProductCustomization.setOnGroupClickListener(grpClick);
        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ProductCustomizationExpListAdapter adapter =
                        (ProductCustomizationExpListAdapter) exLstProductCustomization.getExpandableListAdapter();
                for (int i = 0; i < adapter.getGroupCount(); i++) {
                    for (Product p : adapter.getGroup(i).getProductList()) {
                        if (p.isSelected()) {
                            customizationList.add(new Customization(p.getOutlet_product_id(), p.getId(), p.getDefault_parent_outlet_product_id(), p.getPrice()));
                        }
                    }
                }
                product.setCustomization(customizationList);
                for (int i = 0; i < Integer.parseInt(txtQuantityVal.getText().toString()); i++) {
                    cart.addToCart(companyId, outletId, product);
                }

                product.setComments(edtCommentsBox.getText().toString());

                getActivity().onBackPressed();
            }
        });
    }
    else{
        final Product product= dbMan.getProductByOPMID(outletProductId);//getProductByOutletProductId(outletProductId);
        Toolbox.changeActionBarTitle(getActivity(),"Customize Your Order",product.getName());
        //final List<Customization> customizationList=new ArrayList<Customization>();
        exLstProductCustomization.setAdapter(new ProdNoCustExpAdapter(getActivity(), outletProductId));
        addFooterView();
        txtAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for(int i=0;i<Integer.parseInt(txtQuantityVal.getText().toString());i++){
//                        product.setPrice(0.0d);
                    cart.addToCart(companyId,outletId,product);
                }
                product.setComments(edtCommentsBox.getText().toString());
                getActivity().onBackPressed();
            }
        });
    }

    imgMinus.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            int qty=Integer.parseInt(txtQuantityVal.getText().toString());

            if(qty>1){
                txtQuantityVal.setText(""+(qty-1));
                Toolbox.animateQuantityDecrease(txtQuantityVal);
            }
        }
    });
    imgPlus.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            int qty=Integer.parseInt(txtQuantityVal.getText().toString());

            txtQuantityVal.setText(""+(qty+1));
            Toolbox.animateQuantityIncrease(txtQuantityVal);


        }
    });
    Toolbox.changeActionBarButton(getActivity(),Constants.AB_BTN_BACK);

}
    private void addFooterView(){
        if(exLstProductCustomization.getVisibility()==View.VISIBLE){
        if(footerview==null) {
            footerview = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.customization_list_footer, null);
            edtCommentsBox = (EditText) footerview.findViewById(R.id.edt_product_comments);
            exLstProductCustomization.addFooterView(footerview);
        }
        }
    }
    @Override
    public void onStart() {
        super.onStart();
//        }
    }

    @Override
    public void onResume() {
        super.onResume();

        InputMethodManager imm = (InputMethodManager)this.getActivity().getSystemService(
                Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(edtCommentsBox.getWindowToken(), 0);

    }

    @Override
    public void onPause() {
        super.onPause();

        InputMethodManager imm = (InputMethodManager)this.getActivity().getSystemService(
                Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(edtCommentsBox.getWindowToken(), 0);
    }
ExpandableListView.OnGroupClickListener grpClick=new ExpandableListView.OnGroupClickListener() {
    @Override
    public boolean onGroupClick(ExpandableListView expandableListView, View view, int i, long l) {
        return true;
    }};
}


