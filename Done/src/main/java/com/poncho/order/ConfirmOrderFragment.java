package com.poncho.order;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.citruspay.citruspaylib.model.Address;
import com.citruspay.citruspaylib.model.Card;
import com.citruspay.citruspaylib.model.Customer;
import com.citruspay.citruspaylib.model.ExtraParams;
import com.citruspay.citruspaylib.service.CitrusHttpResponse;
import com.citruspay.citruspaylib.utils.CitrusParams;
import com.poncho.MainActivity;
import com.poncho.R;
import com.poncho.common.TopAndBottomBarFragment;
import com.poncho.payments.WebViewScreen;

import org.apache.http.NameValuePair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;
import to.done.lib.Constants;
import to.done.lib.cart.Cart;
import to.done.lib.database.DBManager;
import to.done.lib.entity.nearestoutlets.Outlet;
import to.done.lib.entity.order.Applied_extra_charge;
import to.done.lib.entity.order.Applied_offer;
import to.done.lib.entity.order.DataForSaveCart;
import to.done.lib.entity.order.Order;
import to.done.lib.entity.order.RespCheckOrder;
import to.done.lib.entity.order.RespSaveOrder;
import to.done.lib.entity.outletsubareas.Area;
import to.done.lib.entity.outletsubareas.Subarea;
import to.done.lib.entity.user.User;
import to.done.lib.sync.SyncListener;
import to.done.lib.sync.SyncManager;
import to.done.lib.ui.CustomDialog;
import to.done.lib.ui.DialogListener;
import to.done.lib.utils.GoogleAnalyticsManager;
import to.done.lib.utils.SharedPreferencesManager;
import to.done.lib.utils.Toolbox;

/**
 * Created by HP on 5/2/2014.
 */
public class ConfirmOrderFragment extends TopAndBottomBarFragment {
    @InjectView(R.id.lst_order_details)
    ListView order_details_list;
    static String generatedHMAC, currencyValue;
    LinearLayout extra_charges_holder;
    LinearLayout applied_offers;
    User user;
    Cart cart;
    ArrayList<Order>orders;
    OrderListAdapter detailsListAdapter;
    double totalAmount=0;
    HashMap<Long,Order>ordersMap;
    RespCheckOrder resp;
    Outlet outlet=new Outlet();
    View footerView=null;
    ImageView img;
TextView taxAmt;
    Subarea userSubarea;
    Area userArea;
    DBManager db;
    ProgressDialog pDialog;
    SharedPreferences.Editor editor;
    SharedPreferences preferencesManager;
    @InjectView(R.id.rg_payments)
    RadioGroup paymentsHolder;
    boolean isOnlinePayment=false;
    int selected_id=-1;
    public static final int REQUEST_CODE_WEB=111,RESUKT_CODE_SUCC=0,RESULT_CODE_FAIL=1;
    LayoutInflater inflater;
    boolean placed=false;
    StringBuilder request=new StringBuilder();
    private ExtraParams extraParams;
    private Customer customer;
    private Address address;
    private Card card;
     Long oid,order_id_for_payment;
    String txn_id,payment_mode;
    private DataForSaveCart save_cart_data;
    String final_pay_amount=null;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =inflater.inflate(R.layout.order_confirmation_page,null);
        ButterKnife.inject(this, view);
        super.onCreateView(inflater,container,savedInstanceState);
        init();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity)getActivity()).drawerOpen=false;
     //   ((MainActivity)getActivity()).writeToSD();


    }

    private void changeHMACRequest(){
        request=new StringBuilder();
        request.append(com.citruspay.citruspaylib.utils.Constants.VANITYURL_VAL+final_pay_amount+txn_id+ com.citruspay.citruspaylib.utils.Constants.CURRENCY_VALUE);
        Toolbox.writeToLog("Request....." + request.toString());
    }

    RadioGroup.OnCheckedChangeListener paymentModeSelect=new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup radioGroup, int i) {
            selected_id=radioGroup.getCheckedRadioButtonId();
            switch (selected_id){
                case R.id.rad_cod:
                    isOnlinePayment=false;
                    payment_mode=Constants.PAYMENT_TYPE_COD;
                    break;
                case R.id.rad_online:
                    isOnlinePayment=true;
                    payment_mode=Constants.PAYMENT_TYPE_ONLINE;
                    break;
                default:break;
            }
        }
    };

    @Override
    public void onProceed() {
        if(selected_id!=-1){
        if(!isOnlinePayment) {
            if (!placed) {
                placed = true;
                getImgProceed().setOnClickListener(null);
                SyncManager.saveOrder(getActivity(), saveOrder, Toolbox.getAppVersionName(getActivity()));
            }
        }else{
            proceedForOnlinePayment();
        }}else{
            showError(ErrorType.ANNOUNCEMENT, "Please select payment type");
        }
        Toolbox.writeToLog("Can place order now?" + (placed));
//        else{
//            Cart.getInstance().clearCart();
//            List<String> backStackEntryList=new ArrayList<String>();
//            for(int i=0;i<getActivity().getSupportFragmentManager().getBackStackEntryCount();i++){
//                android.support.v4.app.FragmentManager.BackStackEntry  entry=getActivity().getSupportFragmentManager().getBackStackEntryAt(i);
//                backStackEntryList.add(entry.getName());
//                // Toolbox.writeToLog("Backstack@"+i+ "= "+entry.getName());
//            }
//            if(backStackEntryList.indexOf("screen_id_"+Constants.SCREEN_OUTLETS)!=-1) {
//                getActivity().getSupportFragmentManager().popBackStackImmediate("screen_id_" + Constants.SCREEN_OUTLETS, 0);
//            }
////            editor.putString(Constants.ORDER_NUMBER,Long.toString(oid));
////            editor.commit();
//            Toolbox.changeScreen(getActivity(), Constants.SCREEN_THANKS,true,null);
//        }
    }

    @Override
    public String getProceedButtonText() {
        return "Confirm";
    }

    private void init(){
        inflater=getActivity().getLayoutInflater();
        pDialog= new ProgressDialog(getActivity());
        pDialog.setCancelable(false);
        if(getActivity()==null) {return;}
        Toolbox.writeToLog("Can place order now?" + (!placed));
        ((MainActivity)getActivity()).drawerOpen=false;

    ordersMap= new HashMap<Long, Order>();
        db= DBManager.getInstance(getActivity());
        if(getActivity()!=null)
            preferencesManager = SharedPreferencesManager.getSharedPreferences(getActivity());
        editor = preferencesManager.edit();
    Toolbox.changeActionBarTitle(getActivity(), "Confirm order", "");
        ImageView img=(ImageView)((ActionBarActivity)getActivity()).getSupportActionBar().getCustomView().findViewById(R.id.imgBack);
        img.setVisibility(View.VISIBLE);
    cart= Cart.getInstance();
        if(cart==null || cart.getOrders()==null || cart.getRespCheckOrder()==null || cart.getUser()==null) {
         //  forceChangetoOutletScreen();
            getActivity().finish();
        }
    resp=cart.getRespCheckOrder();
        if(resp==null || resp.getData()==null){
            SyncManager.checkOrder(getActivity(), checkOrder);
        }
        else {
        initAll();
        }
        extraParams=new ExtraParams();
    paymentsHolder.setOnCheckedChangeListener(paymentModeSelect);

    }

    private void initAll(){
        user = cart.getUser();

        for (Order o : resp.getData().getOrders()) {
            ordersMap.put(o.getOutlet_id(), o);
        }
        orders = new ArrayList<Order>();
        orders.addAll(resp.getData().getOrders());
        if (detailsListAdapter == null || order_details_list.getAdapter()==null) {
            detailsListAdapter = new OrderListAdapter(getActivity(), 6872L, cart.getFirstOrder().getOutlet_id());
        }
//        }else{
////            for(int i=0;i<order_details_list.getHeaderViewsCount();i++){
////                order_details_list.rem
////            }
//            detailsListAdapter.notifyDataSetChanged();
//        }
        if (order_details_list.getHeaderViewsCount() == 0)
            setOrderDetailsListHeader();
        if (order_details_list.getFooterViewsCount() == 0)
            setOrderDetailsFooterView();
        order_details_list.setAdapter(detailsListAdapter);
        for (Order o : orders) {
            totalAmount += o.getTotal();
        }
    }

   View.OnClickListener proceedListener= new View.OnClickListener() {
       @Override
       public void onClick(View view) {

       }
   };

    private void setOrderDetailsListHeader() {
        View headerView = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.od_list_header, null);
        TextView name = (TextView) headerView.findViewById(R.id.txt_user_name);
        TextView phone = (TextView) headerView.findViewById(R.id.txt_user_phone);
        TextView address = (TextView) headerView.findViewById(R.id.txt_useraddress);
        TextView label2 = (TextView) headerView.findViewById(R.id.txt_del_addr_label);
        name.setText(user.getName());
        phone.setText(user.getPrimary_phone());
        if (user.getAddress() != null){
            if (user.getAddress().getArea_id() != null) {
                userArea = db.getAreaById(user.getAddress().getArea_id());
                if (user.getAddress().getSubarea_id() != null) {
                    userSubarea = db.getSubareaById(user.getAddress().getSubarea_id());
                }
            }
        String addr = user.getAddress().getFlat_no() + "," + user.getAddress().getStreet();
            if(user.getAddress().getLandmark()!=null && user.getAddress().getLandmark().trim().length()>0)
                addr=addr+ "," + user.getAddress().getLandmark();
        if (userArea != null && userSubarea != null) {
            addr = addr + "," + userSubarea.getSubarea_name() + "," +userArea.getArea_name() ;
        }
        address.setText(addr);
//            Toolbox.writeToLog("Address area "+userArea.getArea_name()+" subarea "+userSubarea.getSubarea_name());
    }
        else {
            address.setVisibility(View.GONE);
            label2.setVisibility(View.GONE);
        }
        order_details_list.addHeaderView(headerView);
    }

    private void setOrderDetailsFooterView(){
        footerView=((LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.od_list_footer,null);
//         price= (TextView) footerView.findViewById(R.id.price);
        ImageView proceed= (ImageView) footerView.findViewById(R.id.img_done_logo);
        Button redeem_coupon=(Button) footerView.findViewById(R.id.btn_rdm_cpn);
       TextView delCharges,taxText;
        img=(ImageView)footerView.findViewById(R.id.img_done_logo);
//        taxText=(TextView) footerView.findViewById(R.id.txt_payment_method);
        taxAmt=(TextView) footerView.findViewById(R.id.tax_amt);
        delCharges=(TextView) footerView.findViewById(R.id.del_charges_amt);

        proceed.setOnClickListener(proceedListener);
        taxAmt.setText("₹"+resp.getData().getTotal_amount());
        final_pay_amount=resp.getData().getTotal_amount().toString();
        Toolbox.animateQuantityIncrease(taxAmt);
         extra_charges_holder= (LinearLayout) footerView.findViewById(R.id.discounts);
        applied_offers=(LinearLayout) footerView.findViewById(R.id.offers);
//        if(resp.getData().getOrders().get(0).getApplied_extra_charges()!=null && resp.getData().getOrders().get(0).getApplied_extra_charges().size()>0) {
//
//
//            price.setText("+₹ " + resp.getData().getOrders().get(0).getApplied_extra_charges().get(0).getExtra_charged_amt());
//            if(resp.getData().getOrders().get(0).getApplied_extra_charges().get(0).getType().toLowerCase().contains("perce")){
//                taxText.setText("Taxes applicable "+resp.getData().getOrders().get(0).getApplied_extra_charges().get(0).getValue()+"%");
//            }else{
//                taxText.setText("Taxes applicable "+"+₹ "+resp.getData().getOrders().get(0).getApplied_extra_charges().get(0).getValue());
//            }
//        }else {
//            price.setText("+₹ " + 00);
//            taxText.setText("Taxes applicable "+"+₹ "+00);
//        }//resp.getData().getApplied_extra_charges().get(0).getExtra_charged_amt()
//
//        delCharges.setText("+₹ 00");
        redeem_coupon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                RedeemCouponDialogFragment redeemDialog=new RedeemCouponDialogFragment();
//                redeemDialog.show(getActivity().getSupportFragmentManager(),"");
        createCouponDialog();
            }
        });
        if(resp.getData().getOrders().get(0).getApplied_offer()!=null && resp.getData().getOrders().get(0).getApplied_offer().size()>0) {
            if (resp.getData().getOrders().get(0).getApplied_offer().get(0) != null) {
//                discountText.setVisibility(View.VISIBLE);
//                discountAmt.setVisibility(View.VISIBLE);
                Applied_offer aof = resp.getData().getOrders().get(0).getApplied_offer().get(0);
                //Toolbox.showToastLong(getActivity(), "Offer " + aof.getId() + " " + aof.getCoupon_code() + " " + aof.getName());
//                discountText.setText(aof.getName());
//                discountAmt.setText("-₹ "+aof.getDiscount_amount());

            }
        }
        addExtraChargesViewToLayout(extra_charges_holder,resp.getData().getOrders().get(0).getApplied_extra_charges());
        order_details_list.addFooterView(footerView);

    }

    @Override
    public void onStart() {
        super.onStart();
        GoogleAnalyticsManager.sendScreenView(getActivity(), Constants.GA_SCREEN_CONFIRM_ORDER);
    }
    private void forceChangetoOutletScreen(){
        List<String> backStackEntryList = new ArrayList<String>();
        for (int i = 0; i < getActivity().getSupportFragmentManager().getBackStackEntryCount(); i++) {
            android.support.v4.app.FragmentManager.BackStackEntry entry = getActivity().getSupportFragmentManager().getBackStackEntryAt(i);
            backStackEntryList.add(entry.getName());
        }
        if (backStackEntryList.indexOf("screen_id_" + Constants.SCREEN_OUTLETS) != -1) {
            getActivity().getSupportFragmentManager().popBackStackImmediate("screen_id_" + Constants.SCREEN_OUTLETS, 0);
        }

        Toolbox.changeScreen(getActivity(), Constants.SCREEN_THANKS, true, null);
    }

    @Override
    public int getScreenIndex() {
        return TopAndBottomBarFragment.ADDRESS;
    }

    SyncListener saveOrder= new SyncListener() {
        @Override
        public void onSyncStart() {

            imgProceed.setOnClickListener(null);
            if(!pDialog.isShowing()){
                pDialog.setMessage("Placing your order....");
                pDialog.show();
            }else{
                pDialog.dismiss();
            }
           // Toolbox.showToastShort(getActivity(), "Calling save-order");

        }

        @Override
        public void onSyncProgress(float percentProgress, long requestTimestamp) {

        }

        @Override
        public void onSyncSuccess(String url, Object responseObject, long requestTimestamp) {
            Toolbox.writeToLog("Can place order now?" + (!placed));
            if(getActivity()==null)return;
            RespSaveOrder response=(RespSaveOrder)responseObject;

            if(pDialog.isShowing()) {
                pDialog.dismiss();
            }
             oid=response.getData().getOrder_number();
            String message="Order placed succesfully.Your order number is "+oid;
            showError(ErrorType.ANNOUNCEMENT,message, Color.parseColor("#68B861"));
            editor.putString(Constants.ORDER_NUMBER,Long.toString(oid));
            editor.commit();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    hideError();
                }
            },200);
//            placed=true;
            if(response.getResponseCode()== Constants.RESPONSE_CODE_SUCCESS) {
                CustomDialog.createCustomDialog(getActivity(), message, "OK", null, null, false, new DialogListener() {
                    @Override
                    public void onPositiveBtnClick() {
                        Cart.getInstance().clearCart();
//                            MainActivity.writeToSD();
                        List<String> backStackEntryList = new ArrayList<String>();
                        for (int i = 0; i < getActivity().getSupportFragmentManager().getBackStackEntryCount(); i++) {
                            android.support.v4.app.FragmentManager.BackStackEntry entry = getActivity().getSupportFragmentManager().getBackStackEntryAt(i);
                            backStackEntryList.add(entry.getName());
                            // Toolbox.writeToLog("Backstack@"+i+ "= "+entry.getName());
                        }
                        if (backStackEntryList.indexOf("screen_id_" + Constants.SCREEN_OUTLETS) != -1) {
                            getActivity().getSupportFragmentManager().popBackStackImmediate("screen_id_" + Constants.SCREEN_OUTLETS, 0);
                        }
                        editor.putString(Constants.ORDER_NUMBER, Long.toString(oid));
                        editor.commit();
                        Toolbox.changeScreen(getActivity(), Constants.SCREEN_THANKS, true, null);
                    }

                    @Override
                    public void onNegativeBtnClick() {

                    }

                    @Override
                    public void onMiddleBtnClick() {

                    }
                });
            }else {
                if (response.getResponseCode() == Constants.RESP_CODE_PRICE_CHANGE) {
                    CustomDialog.createCustomDialog(getActivity(), "There is a mismatch in the prices of products that" +
                            " you have and /n the actual prices.Please press ok and select your outlet again.", "OK", null, null, false, new DialogListener() {
                        @Override
                        public void onPositiveBtnClick() {
                            Cart.getInstance().clearCart();
                            List<String> backStackEntryList = new ArrayList<String>();
                            for (int i = 0; i < getActivity().getSupportFragmentManager().getBackStackEntryCount(); i++) {
                                android.support.v4.app.FragmentManager.BackStackEntry entry = getActivity().getSupportFragmentManager().getBackStackEntryAt(i);
                                backStackEntryList.add(entry.getName());

                            }
                            if (backStackEntryList.indexOf("screen_id_" + Constants.SCREEN_OUTLETS) != -1) {
                                getActivity().getSupportFragmentManager().popBackStackImmediate("screen_id_" + Constants.SCREEN_OUTLETS, 0);
                            }

                            Toolbox.changeScreen(getActivity(), Constants.SCREEN_THANKS, true, null);
                        }

                        @Override
                        public void onNegativeBtnClick() {

                        }

                        @Override
                        public void onMiddleBtnClick() {

                        }
                    });
                }
            }
      }

        @Override
        public void onSyncFailure(String url, String reason, long requestTimestamp) {
            if(pDialog.isShowing()) {
                pDialog.dismiss();
            }
            showError(ErrorType.ANNOUNCEMENT, reason, Color.RED);
            getImgProceed().setOnClickListener(getProceedClickListener());
            placed=false;
            Toolbox.writeToLog("Can place order now?" + (!placed));
        }

    };

    private ExpandableListView.OnGroupClickListener groupClick= new ExpandableListView.OnGroupClickListener() {
        @Override
        public boolean onGroupClick(ExpandableListView expandableListView, View view, int i, long l) {
            return true;
        }
    };
    private void createCouponDialog(){
        //shift dialogfragment here,easier to refresh page.
        final Dialog dialog= new Dialog(this.getActivity());

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.redeem_coupon_dialog);

        Window window = dialog.getWindow();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.gravity = Gravity.TOP ;

        lp.y = 80;
        window.setAttributes(lp);

        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);

        Button okButton = (Button) dialog.findViewById(R.id.redeem_coupon_ok_button);
        final EditText redeem_coupon_text = (EditText) dialog.findViewById(R.id.redeem_coupon_text);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String coupon = redeem_coupon_text.getText().toString();
                if (coupon != null && !coupon.isEmpty()) {
                    Cart cart = Cart.getInstance();
                    //cart.setCouponCode(coupon);
                    Map<Long, Order> orders = cart.getOrders();
                    for (long outletId : orders.keySet()) {
                        Order o = orders.get(outletId);

                        if (o != null) {
                         cart.setCouponCode(coupon);
                            o.setCoupon_code(coupon);
                        }
                    }

                    SyncManager.checkOrder(getActivity(), checkOrder);
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(redeem_coupon_text.getWindowToken(), 0);
                    dialog.dismiss();
                }
            }
        });

//        setOnTouchListener(new View.OnTouchListener() {
//
//    @Override
//    public boolean onTouch(View v, MotionEvent event) {
//        Configuration config = getResources().getConfiguration();
//        if (config.hardKeyboardHidden == Configuration.HARDKEYBOARDHIDDEN_NO) {
//            v.requestFocusFromTouch();
//        } else {
//            v.requestFocus();
//            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
//            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
//        }
//        return false;
//    }
//});
        dialog.show();
        redeem_coupon_text.requestFocus();
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

    }
    SyncListener checkOrder = new SyncListener() {
        @Override
        public void onSyncStart() {
            if(!pDialog.isShowing()){
                pDialog.setMessage("Verifying discounts...");
                pDialog.show();
            }else{
                pDialog.dismiss();
            }
            hideError();
        }

        @Override
        public void onSyncProgress(float percentProgress, long requestTimestamp) {

        }

        @Override
        public void onSyncSuccess(String url, Object responseObject, long requestTimestamp) {
            RespCheckOrder response=(RespCheckOrder)responseObject;
            if(getActivity()==null)return;
            initAll();
            if(pDialog.isShowing()) {
                pDialog.dismiss();
            }
            resp= Cart.getInstance().getRespCheckOrder();
            addExtraChargesViewToLayout(extra_charges_holder, Cart.getInstance().getRespCheckOrder().getData().getOrders().get(0).getApplied_extra_charges());
            taxAmt.setText("₹"+resp.getData().getTotal_amount());
            final_pay_amount=resp.getData().getTotal_amount().toString();
            List<Applied_offer>offers= Cart.getInstance().getRespCheckOrder().getData().getOrders().get(0).getApplied_offer();
            if(offers!=null && offers.size()>0) {
 //               if (Cart.getInstance().getRespCheckOrder().getData().getOrders().get(0).getApplied_offer().get(0) != null) {
     //               Applied_offer aof = Cart.getInstance().getRespCheckOrder().getData().getOrders().get(0).getApplied_offer().get(0);
//                    discountText.setVisibility(View.VISIBLE);
//                    discountAmt.setVisibility(View.VISIBLE);
//                    discountText.setText(aof.getName());
//                    discountAmt.setText("-₹ "+aof.getDiscount_amount());
 //                   Toolbox.animateQuantityIncrease(discountAmt);
                addDiscountViewToLayout(applied_offers,offers);
                showError(ErrorType.ANNOUNCEMENT,"Discount applied!");

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            hideError();
                        }
                    },100);

                }

 //               }
        else{
//                discountText.setVisibility(View.VISIBLE);
//                discountAmt.setVisibility(View.VISIBLE);
//                discountText.setText("Discount not applicable");
//                discountAmt.setText("-₹ " + 00);
//                Toolbox.animateQuantityIncrease(discountAmt);
               applied_offers.setVisibility(View.GONE);
                showError(ErrorType.ANNOUNCEMENT,"Discount not applicable");

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        hideError();
                    }
                },100);
            }

            if(Cart.getInstance().getRespCheckOrder().getResponseCode()==600){
                showError(ErrorType.ANNOUNCEMENT, Cart.getInstance().getRespCheckOrder().getResponseMsg(), Color.RED);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        hideError();
                    }
                },100);
            }

             {
                if (response.getResponseCode() == Constants.RESP_CODE_PRICE_MISMATCH) {
                    CustomDialog.createCustomDialog(getActivity(), "There is a mismatch in the prices of products that" +
                                    " you have and /n the actual prices.Please press ok and select your outlet again.", "OK",
                            null, null, false, new DialogListener() {
                                @Override
                                public void onPositiveBtnClick() {
                                    Cart.getInstance().clearCart();
                                    List<String> backStackEntryList = new ArrayList<String>();
                                    for (int i = 0; i < getActivity().getSupportFragmentManager().getBackStackEntryCount(); i++) {
                                        android.support.v4.app.FragmentManager.BackStackEntry entry = getActivity().getSupportFragmentManager().getBackStackEntryAt(i);
                                        backStackEntryList.add(entry.getName());

                                    }
                                    if (backStackEntryList.indexOf("screen_id_" + Constants.SCREEN_OUTLETS) != -1) {
                                        getActivity().getSupportFragmentManager().popBackStackImmediate("screen_id_" + Constants.SCREEN_OUTLETS, 0);
                                    }

                                    Toolbox.changeScreen(getActivity(), Constants.SCREEN_THANKS, true, null);
                                }

                                @Override
                                public void onNegativeBtnClick() {

                                }

                                @Override
                                public void onMiddleBtnClick() {

                                }
                            }
                    );

                }
            }

        }

        @Override
        public void onSyncFailure(String url, String reason, long requestTimestamp) {
            if(pDialog.isShowing()) {
                pDialog.dismiss();
            }
            showError(ErrorType.NO_RESULTS, reason);
//            discountAmt.setVisibility(View.GONE);
//            discountText.setVisibility(View.GONE);
            applied_offers.setVisibility(View.GONE);

        }
    };


    public void addExtraChargesViewToLayout(LinearLayout parent,List<Applied_extra_charge>chargesList){
        if(parent.getChildCount()>0) parent.removeAllViews();
        if(chargesList!=null && chargesList.size()>0){
            for(Applied_extra_charge aec:chargesList) {

                View childHolderView = inflater.inflate(R.layout.order_details_extra_charges_view, null, false);
                TextView chargesText = (TextView) childHolderView.findViewById(R.id.txt_payment_method);
                TextView chargesAmount = (TextView) childHolderView.findViewById(R.id.price);

                if(aec.getType().toLowerCase().equals("percent"))
                    chargesText.setText(aec.getName()+" "+aec.getValue()+"%");
                    else chargesText.setText(aec.getName());
                chargesAmount.setText(" +"+Constants.RUPEE_SYMB+ Toolbox.formatDecimal(aec.getExtra_charged_amt()));

                parent.addView(childHolderView);
            }
        }

    }
    public void addDiscountViewToLayout(LinearLayout parent,List<Applied_offer>offersList){
        if(parent.getChildCount()>0) parent.removeAllViews();
        if(offersList!=null && offersList.size()>0){
            if(parent.getVisibility()==View.GONE) parent.setVisibility(View.VISIBLE);
            for(Applied_offer aof:offersList) {

                View childHolderView = inflater.inflate(R.layout.order_details_extra_charges_view, null, false);
                TextView chargesText = (TextView) childHolderView.findViewById(R.id.txt_payment_method);
                TextView chargesAmount = (TextView) childHolderView.findViewById(R.id.price);
                chargesText.setText(aof.getName());//+" "+aof.getCoupon_code());
                chargesAmount.setText(" -"+Constants.RUPEE_SYMB+ Toolbox.formatDecimal(aof.getDiscount_amount()));

                parent.addView(childHolderView);
            }
        }

    }
public void proceedForOnlinePayment(){
    SyncManager.saveCart(getActivity(),payment_mode,saveCartListener, Toolbox.getAppVersionName(getActivity()));

}

    SyncListener saveCartListener=new SyncListener() {
        @Override
        public void onSyncStart() {
            if(!pDialog.isShowing()){
                pDialog.setMessage("Processing...");
                pDialog.show();
            }else{
                pDialog.dismiss();
            }
            hideError();
        }

        @Override
        public void onSyncProgress(float percentProgress, long requestTimestamp) {

        }

        @Override
        public void onSyncSuccess(String url, Object responseObject, long requestTimestamp) {
            if(pDialog.isShowing()) {
                pDialog.dismiss();
            }
            final DataForSaveCart data= (DataForSaveCart) responseObject;
            save_cart_data=data;
            order_id_for_payment=data.getOrders().get(0).getOrder_id();
            data.getClientPlatform();
            oid=data.getOrderNumber();
            txn_id=data.getTxnId();
            if(data!=null) {
                CustomDialog.createCustomDialog(getActivity(), "Transaction id is :" + txn_id+".Please note this transaction id for reference. ", "OK", null, null, false, new DialogListener() {
                    @Override
                    public void onPositiveBtnClick() {
//                        Intent startPayment = new Intent(getActivity(), TestSSLLibraryCall.class);
//                       (prepareBundleForPayments(data));
//
//                        ConfirmOrderFragment.this.startActivityForResult(startPayment,REQUEST_CODE_WEB);
                        extraParams.setMerchantTxnId(txn_id);
                        //new GetSignatureFromMerchantServer(null).execute();
                        changeHMACRequest();
                        SyncManager.getHMAC(getActivity(),request.toString().trim(),generateHMAC);
                    }

                    @Override
                    public void onNegativeBtnClick() {

                    }

                    @Override
                    public void onMiddleBtnClick() {

                    }
                });


            }

            }

        @Override
        public void onSyncFailure(String url, String reason, long requestTimestamp) {
            if(pDialog.isShowing()) {
                pDialog.dismiss();
            }
            showError(ErrorType.NO_RESULTS, reason);
        }
    };

    private void setCustomerAndCardDetails(){
        customer=new Customer();
        address=new Address();
        card=new Card();
        to.done.lib.entity.user.Address userAddress=cart.getUser().getAddress();
        user=cart.getUser();
        user.setIs_new_user(save_cart_data.getUser().getIs_new_user());
        if(user.getAddress()!=null) {
            address.setAddressStreet1(userAddress.getStreet());
            address.setAddressCity(userAddress.getFlat_no() + "," + userAddress.getSociety() + "," + userAddress.getLandmark());
            address.setAddressZip("");
            address.setAddressState("");
        }
        customer.setEmail(user.getPrimary_email());
        customer.setPhoneNumber(user.getPrimary_phone());
        customer.setFirstName(user.getName());
        customer.setLastName("");
    }

    private Bundle prepareBundleForPayments(DataForSaveCart data){
        Bundle bundle=new Bundle();

//        bundle.putSerializable(CitrusParams.PARAM_CUSTOMER,customer);
//        bundle.putSerializable(CitrusParams.PARAM_ADDRESS,address);
//        bundle.putSerializable(CitrusParams.PARAM_CARD,card);
//        ExtraParams params=new ExtraParams();
//        params.setCurrency(com.citruspay.citruspaylib.utils.Constants.CURRENCY_VALUE);
//        params.setMerchantTxnId(data.getTxnId());
//        params.setOrderAmountValue("1.0");
//        //params.setOrderAmountValue(resp.getData().getTotal_amount().toString());
//        params.setReturnUrl(com.citruspay.citruspaylib.utils.Constants.CITRUS_PAY_RETURN_URL);
//        bundle.putSerializable(CitrusParams.PARAM_EXTRAS,params);
        bundle.putString(CitrusParams.PARAM_ORDER_NO,String.valueOf(data.getOrderNumber()));
        bundle.putString(CitrusParams.PARAM_USER_ID,String.valueOf(data.getUser().getId()));
        bundle.putString(CitrusParams.PARAM_NEW_USER,user.getIs_new_user().toString());
        bundle.putString(CitrusParams.PARAM_PLATFORM,data.getClientPlatform());
        bundle.putString(CitrusParams.PARAM_ORDERID,order_id_for_payment.toString());
        return bundle;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
     //   super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==REQUEST_CODE_WEB){
           switch (resultCode) {
               case RESUKT_CODE_SUCC:
                   handleSuccessCase();
                   break;
               case RESULT_CODE_FAIL:
                   handleFailureCase();
                   break;
               default:
                   break;
           }

        }
    }


    private void handleSuccessCase(){

        String message="Order placed succesfully.Your order number is "+oid;
        showError(ErrorType.ANNOUNCEMENT,message, Color.parseColor("#68B861"));
        editor.putString(Constants.ORDER_NUMBER,Long.toString(oid));
        editor.commit();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                hideError();
            }
        },200);
        CustomDialog.createCustomDialog(getActivity(), message, "OK", null, null, false, new DialogListener() {
            @Override
            public void onPositiveBtnClick() {
                Cart.getInstance().clearCart();
//                            MainActivity.writeToSD();
                List<String> backStackEntryList = new ArrayList<String>();
                for (int i = 0; i < getActivity().getSupportFragmentManager().getBackStackEntryCount(); i++) {
                    android.support.v4.app.FragmentManager.BackStackEntry entry = getActivity().getSupportFragmentManager().getBackStackEntryAt(i);
                    backStackEntryList.add(entry.getName());
                    // Toolbox.writeToLog("Backstack@"+i+ "= "+entry.getName());
                }
                if (backStackEntryList.indexOf("screen_id_" + Constants.SCREEN_OUTLETS) != -1) {
                    getActivity().getSupportFragmentManager().popBackStackImmediate("screen_id_" + Constants.SCREEN_OUTLETS, 0);
                }
                editor.putString(Constants.ORDER_NUMBER, Long.toString(oid));
                editor.commit();
                Toolbox.changeScreen(getActivity(), Constants.SCREEN_THANKS, true, null);
            }

            @Override
            public void onNegativeBtnClick() {

            }

            @Override
            public void onMiddleBtnClick() {

            }
        });
    }


    private void handleFailureCase(){
        String message="Payment could not be completed.Your transaction number was "+txn_id;
        CustomDialog.createCustomDialog(getActivity(), message, "OK", null, null, true, null);
    }



    private class GetSignatureFromMerchantServer extends
            AsyncTask<Void, Void, String> {
        private ProgressDialog progressDialog;
        List<NameValuePair> nameValuePairs;

        public GetSignatureFromMerchantServer(List<NameValuePair> nameValuePairs) {
            this.nameValuePairs = nameValuePairs;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setTitle("Loading...");
            progressDialog.setMessage("Authenticating Transaction");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            //	generatedHMAC=merchantTxnIdValue;

            generatedHMAC= CitrusHttpResponse.communicateToServer(com.citruspay.citruspaylib.utils.Constants.HMAC_URL,
                    nameValuePairs, request.toString()).trim();

            Log.d("gaurav", "hmac " + generatedHMAC);
            extraParams.setHMACValue(generatedHMAC);
            extraParams.setOrderAmountValue(final_pay_amount);

            //params.setOrderAmountValue(resp.getData().getTotal_amount().toString());
            extraParams.setReturnUrl(com.citruspay.citruspaylib.utils.Constants.CITRUS_PAY_RETURN_URL);

            return generatedHMAC;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            Log.d("gaurav", "result==" + result);
            if (result != null && !result.contains("<html>")) {
                Intent launchIntent = new Intent(getActivity(),
                        WebViewScreen.class);
                setCustomerAndCardDetails();
                launchIntent.putExtra(CitrusParams.PARAM_CUSTOMER, customer);
                launchIntent.putExtra(CitrusParams.PARAM_ADDRESS, address);
                launchIntent.putExtra(CitrusParams.PARAM_CARD, card);
                launchIntent.putExtra(CitrusParams.PARAM_EXTRAS, extraParams);
                launchIntent.putExtra("custom_params", prepareBundleForPayments(save_cart_data));
//                launchIntent.putStringExtra(CitrusParams.PARAM_ORDER_NO,String.valueOf(data.getOrderNumber()));
//                launchIntent.putString(CitrusParams.PARAM_USER_ID,String.valueOf(data.getUser().getId()));
//                launchIntent.putString(CitrusParams.PARAM_NEW_USER,String.valueOf(0));
//                launchIntent.putString(CitrusParams.PARAM_PLATFORM,data.getClientPlatform());
                startActivityForResult(launchIntent, REQUEST_CODE_WEB);
                //TestSSLLibraryCall.this.finish();


            }

            else {
                Toast.makeText(getActivity(),
                        "Server not responding. Please Try again later.",
                        Toast.LENGTH_SHORT).show();
            }

            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }

        }
    }

    private SyncListener generateHMAC=new SyncListener() {
        @Override
        public void onSyncStart() {
            pDialog.setMessage("Authenticating...");
            pDialog.show();


        }

        @Override
        public void onSyncProgress(float percentProgress, long requestTimestamp) {

        }

        @Override
        public void onSyncSuccess(String url, Object responseObject, long requestTimestamp) {
            String hmac= (String) responseObject;
            generatedHMAC=hmac.trim();
            extraParams.setReturnUrl(com.citruspay.citruspaylib.utils.Constants.CITRUS_PAY_RETURN_URL);
            extraParams.setHMACValue(generatedHMAC);
            extraParams.setOrderAmountValue(final_pay_amount);
            if(pDialog.isShowing())
                pDialog.dismiss();

          //  if (result != null && !result.contains("<html>")) {
                Intent launchIntent = new Intent(getActivity(),
                        WebViewScreen.class);
                setCustomerAndCardDetails();
                launchIntent.putExtra(CitrusParams.PARAM_CUSTOMER, customer);
                launchIntent.putExtra(CitrusParams.PARAM_ADDRESS, address);
                launchIntent.putExtra(CitrusParams.PARAM_CARD, card);
                launchIntent.putExtra(CitrusParams.PARAM_EXTRAS, extraParams);
                launchIntent.putExtra("custom_params", prepareBundleForPayments(save_cart_data));
//                launchIntent.putStringExtra(CitrusParams.PARAM_ORDER_NO,String.valueOf(data.getOrderNumber()));
//                launchIntent.putString(CitrusParams.PARAM_USER_ID,String.valueOf(data.getUser().getId()));
//                launchIntent.putString(CitrusParams.PARAM_NEW_USER,String.valueOf(0));
//                launchIntent.putString(CitrusParams.PARAM_PLATFORM,data.getClientPlatform());
                startActivityForResult(launchIntent, REQUEST_CODE_WEB);
        }

        @Override
        public void onSyncFailure(String url, String reason, long requestTimestamp) {
            showError(ErrorType.ANNOUNCEMENT,"Authentication failed.");
        }
    };
}
