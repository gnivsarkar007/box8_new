package com.poncho.productselection;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.poncho.R;
import com.poncho.common.FilterExpandableListAdapter;
import com.poncho.common.FilterUpdateListener;

import butterknife.ButterKnife;
import butterknife.InjectView;
import to.done.lib.Constants;

/**
 * Created by dipenpradhan on 4/17/14.
 */
public class ProductsNavDrawerContentFrag extends Fragment implements FilterUpdateListener{

    @InjectView(R.id.ex_lst_filters)
    ExpandableListView exLstFilters;
    ProductsNavDrawerExpListAdapter productsNavDrawerExpListAdapter=null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v=inflater.inflate(R.layout.outlets_nav_drawer,null);

        ButterKnife.inject(this,v);

        long outletId=getArguments().getLong(Constants.OUTLET_ID);
        if(productsNavDrawerExpListAdapter==null) {
            FilterExpandableListAdapter.shouldPerformClick=false;
         productsNavDrawerExpListAdapter=new ProductsNavDrawerExpListAdapter(getActivity(),outletId,null);
          exLstFilters.setAdapter(productsNavDrawerExpListAdapter);
        }else{
            FilterExpandableListAdapter.shouldPerformClick=true;
        productsNavDrawerExpListAdapter.notifyDataSetChanged();
            productsNavDrawerExpListAdapter.getFilterUpdateListener().onFilterUpdate();
        }

        return v;
    }

    @Override
    public void onFilterUpdate() {

    }
}
