package com.poncho.payments;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.citruspay.citruspaylib.CitrusSSLLibrary;
import com.citruspay.citruspaylib.JsHandler;
import com.citruspay.citruspaylib.model.Address;
import com.citruspay.citruspaylib.model.Card;
import com.citruspay.citruspaylib.model.Customer;
import com.citruspay.citruspaylib.model.ExtraParams;
import com.citruspay.citruspaylib.utils.CitrusParams;
import com.citruspay.citruspaylib.utils.Constants;
import com.poncho.R;
import com.poncho.order.ConfirmOrderFragment;
import com.poncho.payments.citrusextraparams.CustomExtraParams;

import org.apache.http.util.EncodingUtils;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Map;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import to.done.lib.utils.Toolbox;

public class WebViewScreen extends Activity {
    WebView _mwebView;
    protected int CURRENT_SDK_VERSION = Build.VERSION.SDK_INT;
    protected Map<String, String> jsonResponse;
    private final String TAG = getClass().getName();
    private JsHandler _jsHandler;
    boolean is_error;

    CitrusSSLLibrary citrusSSLLibrary;
    private Customer customer;

    private Address address;
    private Card cards;
    private ExtraParams extraParam;
    String fname, lname, email, streetAddress, city, state, country, pinCode,
            mobile, currencyValue, merchantTxnIdValue, orderAmountValue,
            returnUrlValue, hMacUrlValue, paymentMode, issuerCode,
            cardHolderName, cardNumber, expiryMonth, cardType, cvvNumber,
            expiryYear, _mHAC;
    ProgressDialog progressDialog = null;
    CustomExtraParams customParams;

//    public WebViewScreen(Context context) {
//        super(context);
//    }
//
//    protected WebViewScreen(Context context, boolean cancelable, OnCancelListener cancelListener) {
//        super(context, cancelable, cancelListener);
//    }
//
//    public WebViewScreen(Context context, int theme) {
//        super(context, theme);
//    }
    // ProgressBar progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.webview_screen);
        init();
        initWebView(_mwebView);
//        HashMap<String, String> _mAllParameters = new HashMap<String, String>();
//        //  _mAllParameters.put(CitrusParams.PARAM_MERCHANT_KEY,Constants.MERCHANT_AUTH_VALUE);
//        _mAllParameters.put(CitrusParams.PARAM_FIRST_NAME, customer.getFirstName());
//        _mAllParameters.put(CitrusParams.PARAM_LAST_NAME, customer.getLastName());
//        _mAllParameters.put(CitrusParams.PARAM_PHONE_NUMBER, customer.getPhoneNumber());
//        _mAllParameters.put(CitrusParams.PARAM_CURRENCY, "INR");
//        _mAllParameters.put(CitrusParams.PARAM_ORDER_AMOUNT,  TestSSLLibraryCall.orderAmountValue);
//        _mAllParameters.put(CitrusParams.PARAM_HMAC,
//                TestSSLLibraryCall.generatedHMAC);
//        _mAllParameters.put(CitrusParams.PARAM_MERCHANTTXNID,
//                TestSSLLibraryCall.merchantTxnIdValue);
//        _mAllParameters.put(CitrusParams.PARAM_RETURN_URL,Constants.MYCITRUS_SERVER_URL);
//        _mAllParameters.put("notifyUrl","https://www.google.co.in");
        //"http://120.88.39.191:6060/");

      //  String postData = citrusSSLLibrary
        //        .makeWebViewPostParameters(_mAllParameters).trim();


        String postData = this.makeWebViewPostParameters(
                customer, address, cards, extraParam, customParams).trim();
        _mwebView.postUrl(Constants.MYCITRUS_SERVER_URL,
                EncodingUtils.getBytes(postData, "UTF-8"));
        Toolbox.writeToLog("postdata.."+postData+"....encoded "+_mwebView.getUrl());
        Log.d("gaurav", "value==" + citrusSSLLibrary.getWebClientJsResponse());

        // requestToRemoteServer();
    }

    @Override
    public void onBackPressed() {
  if(is_error) {
      super.onBackPressed();
      finish();
  }
    }

    private void init() {
        _mwebView = (WebView) findViewById(R.id.webView1);
        citrusSSLLibrary = new CitrusSSLLibrary();
        Bundle extras = getIntent().getExtras();
        if (extras != null) {

            customer = (Customer) extras
                    .getSerializable(CitrusParams.PARAM_CUSTOMER);
            address = (Address) extras
                    .getSerializable(CitrusParams.PARAM_ADDRESS);
            cards = (Card) extras.getSerializable(CitrusParams.PARAM_CARD);
            extraParam = (ExtraParams) extras
                    .getSerializable(CitrusParams.PARAM_EXTRAS);
            merchantTxnIdValue=extraParam.getMerchantTxnId();
            customParams=new CustomExtraParams();
//            customParams.setNewuser_val(TestSSLLibraryCall.new_user);
//            customParams.setOrder_no_val(TestSSLLibraryCall.order_number);
//            customParams.setUser_id_val(TestSSLLibraryCall.user_id);
//            customParams.setPlatform_val(TestSSLLibraryCall.platform);
            extraParam.setNotifyUrl(Constants.NOTIFY_URL_VALUE);
            Toolbox.writeToLog("Params1....");
            Bundle params=extras.getBundle("custom_params");
            if(params!=null){
                Toolbox.writeToLog("Params1....custom_params");
                customParams.setNewuser_val(params.getString(CitrusParams.PARAM_NEW_USER));
                customParams.setOrder_no_val(params.getString(CitrusParams.PARAM_ORDER_NO));
                customParams.setUser_id_val(params.getString(CitrusParams.PARAM_USER_ID));
                customParams.setPlatform_val(params.getString(CitrusParams.PARAM_PLATFORM));
                customParams.setOrder_id_val(params.getString(CitrusParams.PARAM_ORDERID));

            }

        }

    }

    @SuppressLint("SetJavaScriptEnabled")
    private void initWebView(WebView _mWebView) {

        citrusSSLLibrary.setWebViewPropertiesForMerchantServer(
                WebViewScreen.this, _mWebView);
        progressDialog = new ProgressDialog(WebViewScreen.this);
        progressDialog.setTitle("Loading...");
        progressDialog
                .setMessage("Please wait. Redirecting to Citrus Payment Gateway.");
        progressDialog.setCancelable(false);
        progressDialog.show();
     //   _mwebView.
        _mWebView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                super.onProgressChanged(view, newProgress);

                if (newProgress < 100) {

                }

                if (newProgress == 100) {
                    if (progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }
                    _mwebView.setVisibility(View.VISIBLE);
                }
            }

        });
        _mWebView.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);

            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                Toolbox.writeToLog("onPageFinished() " + url);

                // on payment cancel
                //https://www.citruspay.com/BOX8/NTA4NDkyNzU/payment_cancel_response
           //     if(!url.toLowerCase().contains(Constants.MYCITRUS_SERVER_URL)){
                if (url.toLowerCase().equals(Constants.CITRUS_PAY_RETURN_URL)) {
                //succesful payment
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
//                            Intent open
//                                    = new Intent(WebViewScreen.this, MainActivity.class);
//
//                            startActivity(open);
                            Toolbox.writeToLog("postDelayed()-->success ");
                            WebViewScreen.this.setResult(ConfirmOrderFragment.RESUKT_CODE_SUCC);
                        WebViewScreen.this.finish();
                        }
                    },2000L);

                }else if(url.toLowerCase().contains("payment_cancel_response")){
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
//                            Intent open
//                                    = new Intent(WebViewScreen.this, MainActivity.class);
//
//                            startActivity(open);
                            Toolbox.writeToLog("postDelayed()-->failure ");
                            WebViewScreen.this.setResult(ConfirmOrderFragment.RESULT_CODE_FAIL);
                            WebViewScreen.this.finish();


                        }
                    },500L);
                }
            //}
            }

            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                super.onReceivedError(view, errorCode, description, failingUrl);
                Toolbox.writeToLog("onReceivedError() " + failingUrl);
                Toolbox.writeToLog("onReceivedError()-->description " + description);

            }

            @Override
            public void onReceivedSslError(WebView view,
                                           SslErrorHandler handler, SslError error) {
                super.onReceivedSslError(view, handler, error);
                is_error=true;
            }
        });
        /**Method 1
         * @author gauravgupta This is HashMap calling for Parameters. Here user
         *         can simply pass all key-pair value in Hashmap.
         * */



        /** Method 2
         * @author gauravgupta This is anoter way of calling parameter with key
         *         value pair. Here we have to set and use all serializable
         *         getter setter class. This is useful when merchant have to use
         *         lots of key pair value.
         * */
		/*String postData_2 = citrusSSLLibrary.makeWebViewPostParameters(
				customer, address, cards, extraParam).trim();
		// Log.d("gaurav", "value==" + mCitrusSSLLibrary);

*/

        // _mwebView.loadUrl("file:///android_asset/main.html");

    }

    public static String hmacDigest(String msg, String keyString) {

        String digest = null;
        try {
            SecretKeySpec key = new SecretKeySpec((keyString).getBytes(), "HmacSHA1");
            Mac mac = Mac.getInstance("HmacSHA1");
            mac.init(key);

            byte[] bytes = mac.doFinal(msg.getBytes("UTF-8"));

            StringBuffer hash = new StringBuffer();
            for (int i = 0; i < bytes.length; i++) {
                String hex = Integer.toHexString(0xFF & bytes[i]);
                if (hex.length() == 1) {
                    hash.append('0');
                }
                hash.append(hex);
            }
            digest = Base64.encodeToString(hash.toString().getBytes(), Base64.CRLF);
        } catch (UnsupportedEncodingException e) {
        } catch (InvalidKeyException e) {
        } catch (NoSuchAlgorithmException e) {
        }

        Toolbox.writeToLog("HMAC generated .."+digest.trim());
        return digest.trim();
    }

    public String makeWebViewPostParameters(Customer customer, Address address,
                                            Card card, ExtraParams extraParam,CustomExtraParams params) {
        StringBuilder postParameterValues = new StringBuilder();

        if (customer != null) {
            if (customer.getFirstName() != null)
                postParameterValues
                        .append(addToPostUrl(CitrusParams.PARAM_FIRST_NAME,
                                customer.getFirstName()));
            if (customer.getLastName() != null)
                postParameterValues.append(addToPostUrl(
                        CitrusParams.PARAM_LAST_NAME, customer.getLastName()));
            if (customer.getEmail() != null)
                postParameterValues.append(addToPostUrl(
                        CitrusParams.PARAM_EMAIL, customer.getEmail()));
            if (customer.getPhoneNumber() != null)
                postParameterValues.append(addToPostUrl(
                        CitrusParams.PARAM_PHONE_NUMBER,
                        customer.getPhoneNumber()));

        }
        if (address != null) {
            if (address.getAddressCity() != null)
                postParameterValues.append(addToPostUrl(
                        CitrusParams.PARAM_ADDRESS_CITY,
                        address.getAddressCity()));
            if (address.getAddressCountry() != null)
                postParameterValues.append(addToPostUrl(
                        CitrusParams.PARAM_ADDRESS_COUNTRY,
                        address.getAddressCountry()));
            if (address.getAddressState() != null)
                postParameterValues.append(addToPostUrl(
                        CitrusParams.PARAM_ADDRESS_STATE,
                        address.getAddressState()));
            if (address.getAddressStreet1() != null)
                postParameterValues.append(addToPostUrl(
                        CitrusParams.PARAM_ADDRESS_STREET1,
                        address.getAddressStreet1()));
            if (address.getAddressZip() != null)
                postParameterValues
                        .append(addToPostUrl(CitrusParams.PARAM_ADDRESS_ZIP,
                                address.getAddressZip()));
        }
        if (card != null) {
            if (card.getCardHolderName() != null)
                postParameterValues.append(addToPostUrl(
                        CitrusParams.PARAM_CARD_HOLDER_NAME,
                        card.getCardHolderName()));
            if (card.getCardNumber() != null)
                postParameterValues.append(addToPostUrl(
                        CitrusParams.PARAM_CARD_NUMBER, card.getCardNumber()));
            if (card.getCardType() != null)
                postParameterValues.append(addToPostUrl(
                        CitrusParams.PARAM_CARD_TYPE, card.getCardType()));
            if (card.getCvvNumber() != null)
                postParameterValues.append(addToPostUrl(
                        CitrusParams.PARAM_CVV_NUMBER, card.getCvvNumber()));
            if (card.getExpiryMonth() != null)
                postParameterValues
                        .append(addToPostUrl(CitrusParams.PARAM_EXPIRY_MONTH,
                                card.getExpiryMonth()));
            if (card.getExpiryYear() != null)
                postParameterValues.append(addToPostUrl(
                        CitrusParams.PARAM_EXPIRY_YEAR, card.getExpiryYear()));
            if (card.getPaymentMode() != null)
                postParameterValues
                        .append(addToPostUrl(CitrusParams.PARAM_PAYMENT_MODE,
                                card.getPaymentMode()));

        }
        if (extraParam != null) {
            if (extraParam.getCurrency() != null)
                postParameterValues.append(addToPostUrl(
                        CitrusParams.PARAM_CURRENCY, extraParam.getCurrency()));
            if (extraParam.getHMACValue() != null)
                postParameterValues.append(addToPostUrl(
                        CitrusParams.PARAM_HMAC, extraParam.getHMACValue()));
            if (extraParam.getMerchantTxnId() != null)
                postParameterValues.append(addToPostUrl(
                        CitrusParams.PARAM_MERCHANTTXNID,
                        extraParam.getMerchantTxnId()));
            if (extraParam.getOrderAmountValue() != null)
                postParameterValues.append(addToPostUrl(
                        CitrusParams.PARAM_ORDER_AMOUNT,
                        extraParam.getOrderAmountValue()));
            if (extraParam.getReturnUrl() != null)
                postParameterValues.append(addToPostUrl(
                        CitrusParams.PARAM_RETURN_URL,
                        extraParam.getReturnUrl()));
            if (extraParam.getNotifyUrl() != null)
                postParameterValues.append(addToPostUrl(
                        CitrusParams.PARAM_NOTIFY_URL,
                        extraParam.getNotifyUrl()));

        }
        int i=0;

        String append_string_keys="customParams[0].name="+CitrusParams.PARAM_ORDER_NO+"&"+"customParams[1].name="+CitrusParams.PARAM_NEW_USER+"&"+"customParams[2].name="+CitrusParams.PARAM_USER_ID+"&"+"customParams[3].name="+CitrusParams.PARAM_PLATFORM+"&"+"customParams[4].name="+CitrusParams.PARAM_ORDERID;
        String append_string_vals="customParams[0].value="+params.getOrder_no_val()+"&"+"customParams[1].value="+params.getNewuser_val()+"&"+"customParams[2].value="+params.getUser_id_val()+"&"+"customParams[3].value="+"android"+"&"+"customParams[4].value="+params.getOrder_id_val();
        postParameterValues.append(append_string_keys+"&"+append_string_vals);
        Toolbox.writeToLog("params request..."+postParameterValues.toString());
//        if(params!=null){
//            if(params.getOrder_no_val()!=null){
//                postParameterValues.append(addToPostUrl(
//                        "customParams["+(i)+"].name="+CitrusParams.PARAM_ORDER_NO,
//                        "customParams["+(i)+"].value="+params.getOrder_no_val()));
//            }
//            i++;
//            if(params.getNewuser_val()!=null){
//                postParameterValues.append(addToPostUrl(
//                        "customParams["+(i)+"].name="+CitrusParams.PARAM_NEW_USER,
//                        "customParams["+(i)+"].value="+params.getNewuser_val()));
//            }
//            i++;
//            if(params.getUser_id_val()!=null){
//                postParameterValues.append(addToPostUrl(
//                        "customParams["+(i)+"].name="+CitrusParams.PARAM_USER_ID,
//                        "customParams["+(i)+"].value="+params.getUser_id_val()));
//            }
//            i++;
//            if(params.getPlatform_val()!=null){
//                postParameterValues.append(addToPostUrl(
//                        "customParams["+(i)+"].name="+CitrusParams.PARAM_PLATFORM,
//                        "customParams["+(i)+"].value="+params.getPlatform_val()));
//            }
//        }
        return postParameterValues.toString().trim();
    }

    public String addToPostUrl(String paramKey, String paramValue)

    {
        return paramKey.concat(Constants.PARAMETER_EQUALS).concat(paramValue)
                .concat(Constants.PARAMETER_SEPERATOR);

    }

}
