package com.poncho.payments.citrusextraparams;

/**
 * Created by root on 30/10/14.
 */
public class CustomExtraParams {
    private String order_no_key;
    private String user_id_key;
    private String newuser_key;
    private String platform_key;
    private String order_id_key;

    private String order_no_val;
    private String user_id_val;
    private String newuser_val;
    private String platform_val;
    private String order_id_val;

    public String getOrder_id_key() {
        return order_id_key;
    }

    public void setOrder_id_key(String order_id_key) {
        this.order_id_key = order_id_key;
    }

    public String getOrder_id_val() {
        return order_id_val;
    }

    public void setOrder_id_val(String order_id_val) {
        this.order_id_val = order_id_val;
    }

    public String getOrder_no_key() {
        return order_no_key;
    }

    public void setOrder_no_key(String order_no_key) {
        this.order_no_key = order_no_key;
    }

    public String getUser_id_key() {
        return user_id_key;
    }

    public void setUser_id_key(String user_id_key) {
        this.user_id_key = user_id_key;
    }

    public String getNewuser_key() {
        return newuser_key;
    }

    public void setNewuser_key(String newuser_key) {
        this.newuser_key = newuser_key;
    }

    public String getPlatform_key() {
        return platform_key;
    }

    public void setPlatform_key(String platform_key) {
        this.platform_key = platform_key;
    }

    public String getOrder_no_val() {
        return order_no_val;
    }

    public void setOrder_no_val(String order_no_val) {
        this.order_no_val = order_no_val;
    }

    public String getUser_id_val() {
        return user_id_val;
    }

    public void setUser_id_val(String user_id_val) {
        this.user_id_val = user_id_val;
    }

    public String getNewuser_val() {
        return newuser_val;
    }

    public void setNewuser_val(String newuser_val) {
        this.newuser_val = newuser_val;
    }

    public String getPlatform_val() {
        return platform_val;
    }

    public void setPlatform_val(String platform_val) {
        this.platform_val = platform_val;
    }
}
