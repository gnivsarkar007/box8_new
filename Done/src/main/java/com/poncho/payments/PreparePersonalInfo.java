package com.poncho.payments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.citruspay.citruspaylib.CitrusSSLLibrary;
import com.citruspay.citruspaylib.model.Address;
import com.citruspay.citruspaylib.model.Card;
import com.citruspay.citruspaylib.model.Customer;
import com.citruspay.citruspaylib.model.ExtraParams;
import com.citruspay.citruspaylib.utils.CitrusParams;
import com.citruspay.citruspaylib.utils.Constants;
import com.poncho.R;

import java.math.BigInteger;
import java.util.Random;

public class PreparePersonalInfo extends Activity implements OnClickListener {

	private EditText fname, lname, email, streetAddress, city, state, country,
			pinCode, mobile, hmac, returnurl, paymentMode, issuerCode,
			cardHolderName, cardNumber, expiryMonth, cardType, cvvNumber,
			expiryYear, orderAmount;
	private Button nextNow;
	private final String TAG = getClass().getName();
	Address mAddress;
	ExtraParams mExtraParams;
	Card mCard;
	Customer mCustomer;
	CitrusSSLLibrary citrusSSLLibrary = new CitrusSSLLibrary();
	private TextView merchantTrasactionId, oderAmount;
	private String generateRandomTransactionId;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.ssl_lib_test);

		init();
	}

	@Override
	protected void onResume() {
		super.onResume();
		generateRandomTransactionId ="" ;//optional
		merchantTrasactionId.setText(generateRandomTransactionId);
		oderAmount.setText("0.0");
	}

	private void init() {

		fname = (EditText) findViewById(R.id.efname);
		lname = (EditText) findViewById(R.id.elname);
		email = (EditText) findViewById(R.id.reuname);
		streetAddress = (EditText) findViewById(R.id.readddress);
		city = (EditText) findViewById(R.id.ecity);
		state = (EditText) findViewById(R.id.estate);
		country = (EditText) findViewById(R.id.ecountry);
		pinCode = (EditText) findViewById(R.id.epincode);
		mobile = (EditText) findViewById(R.id.emobile);
		hmac = (EditText) findViewById(R.id.ehac);
		returnurl = (EditText) findViewById(R.id.ereturnurl);
		paymentMode = (EditText) findViewById(R.id.epaymentmode);
		issuerCode = (EditText) findViewById(R.id.eissuercode);
		cardHolderName = (EditText) findViewById(R.id.ecardholdername);
		cardNumber = (EditText) findViewById(R.id.ecardnumber);
		expiryMonth = (EditText) findViewById(R.id.eexpirymonth);
		cardType = (EditText) findViewById(R.id.ecardtype);
		cvvNumber = (EditText) findViewById(R.id.ecvvnumber);
		expiryYear = (EditText) findViewById(R.id.eexpiryyear);
		orderAmount = (EditText) findViewById(R.id.eorderamount);
		nextNow = (Button) findViewById(R.id.next_now);
		nextNow.setOnClickListener(PreparePersonalInfo.this);

		merchantTrasactionId = (TextView) findViewById(R.id.ssl_transaction_num);
		oderAmount = (TextView) findViewById(R.id.ssl_order_amount);
	}

	private void getData() {
		mAddress = new Address();
		mAddress.setAddressStreet1(streetAddress.getText().toString().trim());
		mAddress.setAddressCity(city.getText().toString().trim());
		mAddress.setAddressCountry(country.getText().toString().trim());
		mAddress.setAddressState(state.getText().toString().trim());
		mAddress.setAddressZip(pinCode.getText().toString().trim());

		mCustomer = new Customer();
		mCustomer.setFirstName(fname.getText().toString().trim());
		mCustomer.setLastName(lname.getText().toString().trim());
		mCustomer.setEmail(email.getText().toString().trim());
		mCustomer.setPhoneNumber(mobile.getText().toString().trim());

		mCard = new Card();
		mCard.setCardHolderName(cardHolderName.getText().toString().trim());
		mCard.setCardNumber(cardNumber.getText().toString().trim());
		mCard.setCardType(cardType.getText().toString().trim());
		mCard.setCvvNumber(cvvNumber.getText().toString().trim());
		mCard.setExpiryMonth(expiryMonth.getText().toString().trim());
		mCard.setExpiryYear(expiryYear.getText().toString().trim());

		mExtraParams = new ExtraParams();

		mExtraParams.setReturnUrl(Constants.CITRUS_PAY_RETURN_URL);

		mExtraParams.setOrderAmountValue(orderAmount.getText().toString()
				.trim());

		mExtraParams.setMerchantTxnId(generateRandomTransactionId);

		mExtraParams.setCurrency("INR");
	}

	@Override
	public void onClick(View v) {
		Log.i(TAG, "Next clicked");
		if (v.getId() == R.id.next_now) {

			getData();

			Intent launchIntent = new Intent(PreparePersonalInfo.this,
					TestSSLLibraryCall.class);
			launchIntent.putExtra(CitrusParams.PARAM_CUSTOMER, mCustomer);
			launchIntent.putExtra(CitrusParams.PARAM_ADDRESS, mAddress);
			launchIntent.putExtra(CitrusParams.PARAM_CARD, mCard);
			launchIntent.putExtra(CitrusParams.PARAM_EXTRAS, mExtraParams);

			startActivity(launchIntent);
		}
	}
	
	public String generateRandomMerchantTransactionId() {
		BigInteger b = new BigInteger(64, new Random());
		String tId = b.toString();
		return tId;
	}

}
