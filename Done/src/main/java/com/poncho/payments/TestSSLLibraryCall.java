package com.poncho.payments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.citruspay.citruspaylib.CitrusSSLLibrary;
import com.citruspay.citruspaylib.model.Address;
import com.citruspay.citruspaylib.model.Card;
import com.citruspay.citruspaylib.model.Customer;
import com.citruspay.citruspaylib.model.ExtraParams;
import com.citruspay.citruspaylib.utils.CitrusParams;
import com.citruspay.citruspaylib.utils.Constants;
import com.poncho.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import to.done.lib.utils.Toolbox;

//import com.citruspay.citruspaylib.model.CustomParameters;

public class TestSSLLibraryCall extends Activity implements OnClickListener {

	private final String TAG = getClass().getName();
	private Customer customer;
	private Address address;
	private Card card;
	private ExtraParams extraParams;
	private TextView fname, lname, email, streetAddress, city, state, country,
			pinCode, mobile, hmac, returnurl;
	private Button payNow;
	List<NameValuePair> nameValuePairs;
	private LinearLayout responseLayout;
	private ScrollView payNowView, responseScrollLayout;
	private boolean isDataReceived;
	private Map<String, String> response;
	private LayoutParams MATCH_WRAP_LAYOUT = new LayoutParams(
			LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
	static String merchantTxnIdValue, orderAmountValue, returnUrlValue,order_number,user_id,new_user,platform;
	String vanityURLPart = null;
	static String generatedHMAC, currencyValue;
	public static final int ACTION_WEB_VIEW = 111;
	CitrusSSLLibrary citrusSSLLibrary;
    StringBuilder request=new StringBuilder();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ssl_library_call);
		init();
		getValuesFromIntent();
		payNowView.setVisibility(View.VISIBLE);
		responseScrollLayout.setVisibility(View.GONE);
	}

	private void init() {
		citrusSSLLibrary = new CitrusSSLLibrary();
		responseScrollLayout = (ScrollView) findViewById(R.id.responseScrollLayout);
		responseLayout = (LinearLayout) findViewById(R.id.responseLayout);
		payNowView = (ScrollView) findViewById(R.id.payNowLayout);
		payNow = (Button) findViewById(R.id.btn_pay_now);
		payNow.setOnClickListener(this);
		fname = (TextView) findViewById(R.id.vfname);
		lname = (TextView) findViewById(R.id.vlname);
		email = (TextView) findViewById(R.id.rvuname);
		streetAddress = (TextView) findViewById(R.id.rvadddress);
		city = (TextView) findViewById(R.id.vcity);
		state = (TextView) findViewById(R.id.vstate);
		country = (TextView) findViewById(R.id.vcountry);
		pinCode = (TextView) findViewById(R.id.vpincode);
		mobile = (TextView) findViewById(R.id.vmobile);
		hmac = (TextView) findViewById(R.id.vhmac);
		returnurl = (TextView) findViewById(R.id.vreturnurl);
	}

	private void getValuesFromIntent() {
		Bundle extras = getIntent().getExtras();

		if (extras != null) {
			customer = (Customer) extras
					.getSerializable(CitrusParams.PARAM_CUSTOMER);
			address = (Address) extras
					.getSerializable(CitrusParams.PARAM_ADDRESS);
			card = (Card) extras.getSerializable(CitrusParams.PARAM_CARD);
			extraParams = (ExtraParams) extras
					.getSerializable(CitrusParams.PARAM_EXTRAS);
			
			fname.setText(customer.getFirstName());
			lname.setText(customer.getLastName());
			email.setText(customer.getEmail());
			streetAddress.setText(address.getAddressStreet1());
			city.setText(address.getAddressCity());
			state.setText(address.getAddressState());
			country.setText(address.getAddressCountry());
			pinCode.setText(address.getAddressZip());
			mobile.setText(customer.getPhoneNumber());

			currencyValue = extraParams.getCurrency().toString();
			merchantTxnIdValue = extraParams.getMerchantTxnId().toString();
			orderAmountValue = extraParams.getOrderAmountValue();
			returnUrlValue = extraParams.getReturnUrl();
            //,,,
            order_number=extras.getString(CitrusParams.PARAM_ORDER_NO,"");
            user_id=extras.getString(CitrusParams.PARAM_USER_ID,"");
            new_user=extras.getString(CitrusParams.PARAM_NEW_USER,"");
            platform=extras.getString(CitrusParams.PARAM_PLATFORM,"android");
//            bundle.putString(,String.valueOf(data.getOrderNumber()));
//            bundle.putString(,String.valueOf(data.getUser().getId()));
//            bundle.putString(,String.valueOf(0));
//            bundle.putString(,data.getClientPlatform());
//
//            customParam = new CustomParameters();
//
//            customParam.setFirstCustomParamName(CitrusParams.PARAM_STORE_CODE);
//            customParam.setFirstCustomParamValue(storeCode);
//
//            customParam.setSecondCustomParamName(CitrusParams.PARAM_STORE_NAME);
//            customParam.setSecondCustomParamValue(storeName);
//
//            customParam.setThirdCustomParamName(CitrusParams.PARAM_DEVICE_MODE);
//            customParam.setThirdCustomParamValue(orderMode);
			returnurl.setText(returnUrlValue.toString());

            nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair(
                    CitrusParams.PARAM_VANITY_URL, Constants.VANITY_URL));
            nameValuePairs.add(new BasicNameValuePair(
                    CitrusParams.PARAM_ORDER_AMOUNT, orderAmountValue));
            nameValuePairs.add(new BasicNameValuePair(
                    CitrusParams.PARAM_CURRENCY, Constants.CURRENCY_VALUE));
            nameValuePairs.add(new BasicNameValuePair(Constants.MERCHANTTXNID,
                    merchantTxnIdValue));
        //    CitrusParams.PARAM_MERCHANT_KEY,Constants.MERCHANT_AUTH_VALUE

            request.append(Constants.VANITYURL_VAL+orderAmountValue+merchantTxnIdValue+Constants.CURRENCY_VALUE);
            Toolbox.writeToLog("Request....." + request.toString());
            //generatedHMAC=hmacDigest(request.toString(),"08382498b5bd20c8f28a5641bd8cd7d704c1fb6d");
		}
	}

	private class GetSignatureFromMerchantServer extends
			AsyncTask<Void, Void, String> {
		private ProgressDialog progressDialog;
		List<NameValuePair> nameValuePairs;

		public GetSignatureFromMerchantServer(List<NameValuePair> nameValuePairs) {
			this.nameValuePairs = nameValuePairs;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog = new ProgressDialog(TestSSLLibraryCall.this);
			progressDialog.setTitle("Loading...");
			progressDialog.setMessage("Authenticating Transaction");
			progressDialog.show();
		}

		@Override
		protected String doInBackground(Void... params) {
		//	generatedHMAC=merchantTxnIdValue;
           // generatedHMAC=CitrusHttpResponse.communicateToServer(Constants.HMAC_URL,
            //        nameValuePairs,request.toString()).trim();

            Log.d("gaurav", "hmac " + generatedHMAC);
			extraParams.setHMACValue(generatedHMAC);

			return generatedHMAC;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

			Log.d("gaurav", "result==" + result);
			if (result != null && !result.contains("<html>")) {
				Intent launchIntent = new Intent(TestSSLLibraryCall.this,
						WebViewScreen.class);
				launchIntent.putExtra(CitrusParams.PARAM_CUSTOMER, customer);
				launchIntent.putExtra(CitrusParams.PARAM_ADDRESS, address);
				launchIntent.putExtra(CitrusParams.PARAM_CARD, card);
				launchIntent.putExtra(CitrusParams.PARAM_EXTRAS, extraParams);
//                launchIntent.putStringExtra(CitrusParams.PARAM_ORDER_NO,String.valueOf(data.getOrderNumber()));
//                launchIntent.putString(CitrusParams.PARAM_USER_ID,String.valueOf(data.getUser().getId()));
//                launchIntent.putString(CitrusParams.PARAM_NEW_USER,String.valueOf(0));
//                launchIntent.putString(CitrusParams.PARAM_PLATFORM,data.getClientPlatform());
				startActivityForResult(launchIntent, ACTION_WEB_VIEW);
                TestSSLLibraryCall.this.finish();


			}

			else {
				Toast.makeText(TestSSLLibraryCall.this,
						"Server not responding. Please Try again later.",
						Toast.LENGTH_SHORT).show();
			}

			if (progressDialog != null && progressDialog.isShowing()) {
				progressDialog.dismiss();
			}

		}
	}

    public String addToPostUrl(String paramKey, String paramValue)
    {
        return paramKey.concat(Constants.PARAMETER_EQUALS).concat(paramValue)
                .concat(Constants.PARAMETER_SEPERATOR);
    }
	@Override
	public void onClick(View v) {
		Log.i(TAG, "Pay Now clicked");
		if (v.getId() == R.id.btn_pay_now) {
			new GetSignatureFromMerchantServer(nameValuePairs).execute();
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (isDataReceived) {
			changeTheContentView();
			isDataReceived = false;
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == ACTION_WEB_VIEW) {
			try {
				Intent intent = new Intent(getApplicationContext(),
						ResponseScreen.class);
                Toolbox.writeToLog("JSON response...."+ citrusSSLLibrary.getWebClientJsResponse());
				intent.putExtra("jsvalue", citrusSSLLibrary.getWebClientJsResponse());
                Toolbox.writeToLog("JSON response...."+ citrusSSLLibrary.getWebClientJsResponse());
				startActivity(intent);
				TestSSLLibraryCall.this.finish();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	private void changeTheContentView() {
		if (response != null && response.size() > 0) {
			TextView heading = new TextView(TestSSLLibraryCall.this);
			heading.setLayoutParams(MATCH_WRAP_LAYOUT);
			heading.setText("Transaction Details");
			heading.setTextSize(18);
			responseLayout.addView(heading);
			addFieldsToTheMainLayout("TxId", "Transaction id");
			addFieldsToTheMainLayout("TxStatus", "Transaction status");
			addFieldsToTheMainLayout("TxRefNo", "Transaction reference number");
			addFieldsToTheMainLayout("amount", "Transaction Amount");
			addFieldsToTheMainLayout("TxMsg", "Transaction Message");
			addFieldsToTheMainLayout("transactionId", "Citrus transaction id");
			addFieldsToTheMainLayout(null, "User Details");
			addFieldsToTheMainLayout("firstName", "First name");
			addFieldsToTheMainLayout("lastName", "Last name");
			addFieldsToTheMainLayout("email", "Email");
			addFieldsToTheMainLayout("mobileNo", "Mobile");
			addFieldsToTheMainLayout("addressStreet1", "Address Street");
			addFieldsToTheMainLayout("addressCity", "City");
			addFieldsToTheMainLayout("addressState", "State");
			addFieldsToTheMainLayout("addressZip", "Pincode");
			addFieldsToTheMainLayout("addressCountry", "Country");
			addFieldsToTheMainLayout(null, "Other Details");
			addFieldsToTheMainLayout("pgTxnNo", "PG transaction number");
			addFieldsToTheMainLayout("pgRespCode", "PG response code");
			addFieldsToTheMainLayout("authIdCode", "Authorization id code");
			addFieldsToTheMainLayout("issuerRefNo", "Issuer Reference Number");
			addFieldsToTheMainLayout("productSKU", "Product SKU");
			payNowView.setVisibility(View.GONE);
			responseScrollLayout.setVisibility(View.VISIBLE);
		}
	}

	private void addFieldsToTheMainLayout(String fieldName, String viewHeading) {
		String txStatus = response.get(fieldName);
		TextView textView = new TextView(TestSSLLibraryCall.this);
		textView.setLayoutParams(MATCH_WRAP_LAYOUT);
		if (fieldName != null) {
			textView.setText(Html.fromHtml("<b>" + viewHeading + " : " + "</b>"
					+ "<br />" + "<small>" + txStatus + "</small>"));
		} else {
			textView.setText(Html.fromHtml("<br />" + "<b>" + viewHeading
					+ "</b>"));
		}
		if (txStatus != null && !txStatus.equals("") || fieldName == null)
			responseLayout.addView(textView);
	}

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    public static String hmacDigest(String msg, String keyString) {
Log.e("hmac","hmac string:"+msg);
        String digest = null;
        try {
            SecretKeySpec key = new SecretKeySpec((keyString).getBytes(), "HmacSHA1");
            Mac mac = Mac.getInstance("HmacSHA1");
            mac.init(key);

            byte[] bytes = mac.doFinal(msg.getBytes("UTF-8"));

            StringBuffer hash = new StringBuffer();
            for (int i = 0; i < bytes.length; i++) {
                String hex = Integer.toHexString(0xFF & bytes[i]);
                if (hex.length() == 1) {
                    hash.append('0');
                }
                hash.append(hex);
            }
            digest = Base64.encodeToString(hash.toString().getBytes(), Base64.CRLF);
        } catch (UnsupportedEncodingException e) {
        } catch (InvalidKeyException e) {
        } catch (NoSuchAlgorithmException e) {
        }
        return digest.trim();
    }
}
