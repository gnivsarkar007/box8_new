package com.poncho.payments;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

import com.citruspay.citruspaylib.service.CitrusGetWebClientJSResponse;
import com.poncho.R;

/**
 * Created by root on 27/10/14.
 */
public class ResponseScreen extends Activity{

    TextView mTranscationIdOrderId, mTextMessage, mTxRefNo, mPgTxnNo, mTxStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ssl_lib_response);
        initWidget();
    }

    private void initWidget() {
        mTranscationIdOrderId = (TextView) findViewById(R.id.txid_orderid);
        mTextMessage = (TextView) findViewById(R.id.txmsg);
        mTxRefNo = (TextView) findViewById(R.id.pgtxnno);
        mTxStatus = (TextView) findViewById(R.id.txstatus);
        setJsonValue();
    }

    private void setJsonValue() {
        String jsonResponse = getIntent().getStringExtra("jsvalue");
        CitrusGetWebClientJSResponse citrusGetWebClientJSResponse = new CitrusGetWebClientJSResponse(jsonResponse);
        // optional
        mTranscationIdOrderId.setText("Transaction Number: " + citrusGetWebClientJSResponse.getTranssactionId()
                + " |  Order Amount: " + citrusGetWebClientJSResponse.getAmount() + " "
                + citrusGetWebClientJSResponse.getCurrency());
        mTextMessage.setText(citrusGetWebClientJSResponse.getTxMsg());
        mTxRefNo.setText(citrusGetWebClientJSResponse.getTxRefNo());
        mTxStatus.setText(citrusGetWebClientJSResponse.getTxStatus());
        // citrusGetWebClientJSResponse.ge

    }
}
