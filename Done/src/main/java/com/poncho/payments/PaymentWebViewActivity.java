package com.poncho.payments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Bundle;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.citruspay.citruspaylib.CitrusSSLLibrary;
import com.citruspay.citruspaylib.utils.CitrusParams;
import com.citruspay.citruspaylib.utils.Constants;
import com.poncho.R;

import java.util.HashMap;

import butterknife.ButterKnife;
import butterknife.InjectView;
import to.done.lib.cart.Cart;
import to.done.lib.database.DBManager;
import to.done.lib.utils.Toolbox;

/**
 * Created by root on 21/10/14.
 */
public class PaymentWebViewActivity extends CitrusSSLLibrary {
    @InjectView(R.id.payment_web_view)
    WebView _mwebView;
    Cart mCart;
    DBManager dbManager;
   // CitrusSSLLibrary citrusSSLLibrary;
    ProgressDialog progressDialog = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payment_webview_screen);
        ButterKnife.inject(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        init();
        initWebView();
    }

    private void init(){
        mCart=Cart.getInstance();
        dbManager=DBManager.getInstance(this);
    //    citrusSSLLibrary=new CitrusSSLLibrary();
        progressDialog=new ProgressDialog(this);

    }


    @SuppressLint("SetJavaScriptEnabled")
    private void initWebView() {

        this.setWebViewPropertiesForMerchantServer(
                this, _mwebView);
       // progressDialog = new ProgressDialog(WebViewScreen.this);
        progressDialog.setTitle("Loading...");
        progressDialog
                .setMessage("Please wait. Redirecting to Citrus Payment Gateway.");
        progressDialog.show();

        _mwebView.loadUrl(Constants.MYCITRUS_SERVER_URL);
        _mwebView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                super.onProgressChanged(view, newProgress);

                if (newProgress < 100) {
                    Toolbox.writeToLog("Progress is .."+newProgress+" ..URL is.."+view.getUrl());
                }

                if (newProgress == 100) {
                    if (progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }
                    _mwebView.setVisibility(View.VISIBLE);
                }
            }

        });
        _mwebView.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);


            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);

            }

            @Override
            public void onReceivedSslError(WebView view,
                                           SslErrorHandler handler, SslError error) {
                super.onReceivedSslError(view, handler, error);
            }
        });
        /**Method 1
         * @author gauravgupta This is HashMap calling for Parameters. Here user
         *         can simply pass all key-pair value in Hashmap.
         * */
        HashMap<String, String> _mAllParameters = new HashMap<String, String>();
        _mAllParameters.put(CitrusParams.PARAM_FIRST_NAME, "gaurav");
        _mAllParameters.put(CitrusParams.PARAM_LAST_NAME, "gupta");
        _mAllParameters.put(CitrusParams.PARAM_PHONE_NUMBER, "9350103180");
        _mAllParameters.put(CitrusParams.PARAM_CURRENCY, "INR");
        _mAllParameters.put(CitrusParams.PARAM_ORDER_AMOUNT, "1.0");
        _mAllParameters.put(CitrusParams.PARAM_HMAC,
                TestSSLLibraryCall.generatedHMAC);
        _mAllParameters.put(CitrusParams.PARAM_MERCHANTTXNID,
                TestSSLLibraryCall.merchantTxnIdValue);
        _mAllParameters.put(CitrusParams.PARAM_RETURN_URL,
                "http://120.88.39.191:6060/");

        String postData = this
                .makeWebViewPostParameters(_mAllParameters);

        /** Method 2
         * @author gauravgupta This is anoter way of calling parameter with key
         *         value pair. Here we have to set and use all serializable
         *         getter setter class. This is useful when merchant have to use
         *         lots of key pair value.
         * */
		/*String postData_2 = citrusSSLLibrary.makeWebViewPostParameters(
				customer, address, cards, extraParam).trim();
		// Log.d("gaurav", "value==" + mCitrusSSLLibrary);

		_mwebView.postUrl(Constants.MYCITRUS_SERVER_URL,
				EncodingUtils.getBytes(postData_2, "UTF-8"));*/

        // _mwebView.loadUrl("file:///android_asset/main.html");

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }
}
