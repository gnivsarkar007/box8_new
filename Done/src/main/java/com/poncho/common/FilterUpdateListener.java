package com.poncho.common;

/**
 * Created by dipenpradhan on 4/30/14.
 */
public interface FilterUpdateListener {

    void onFilterUpdate();
}
