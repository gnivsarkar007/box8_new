package com.poncho.common;

/**
 * Created by HP on 8/25/2014.
 */
public interface IOnParentCategoryChangedListener {

    public void parentCategoryChanged(Long current_selected_parent);
}
