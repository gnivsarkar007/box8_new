package com.poncho.cart;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.poncho.MainActivity;
import com.poncho.R;
import com.poncho.common.TopAndBottomBarFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import to.done.lib.Constants;
import to.done.lib.cart.Cart;
import to.done.lib.cart.CartListener;
import to.done.lib.database.DBManager;
import to.done.lib.entity.nearestoutlets.Outlet;
import to.done.lib.ui.CustomDialog;
import to.done.lib.utils.GoogleAnalyticsManager;
import to.done.lib.utils.Toolbox;

/**
 * Created by dipenpradhan on 4/26/14.
 */
public class CartFragment extends TopAndBottomBarFragment {


    @InjectView(R.id.lst_content)
    ListView lstContent;
    @InjectView(R.id.rg_order_delivery_type)
    RadioGroup rgOrderDeliveryType;

    private Cart cart =Cart.getInstance();
    private boolean cartReachedByProceed;
    private DBManager dbMan;
    private String[]deliveryTypes;
    Outlet outlet=null;
    View footerView=null;
    CartListAdapter adp=null;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        cartReachedByProceed =getArguments().getBoolean(Constants.CART_REACHED_BY_PROCEED,false);

        View view=inflater.inflate(R.layout.cart,null);
        ButterKnife.inject(this, view);
        super.onCreateView(inflater,container,savedInstanceState);
        init();
        return view;
    }

    TextView txtCartTotalVal;
    @Override
    public void onStart() {
        super.onStart();
        GoogleAnalyticsManager.sendScreenView(getActivity(), Constants.GA_SCREEN_CART);
    }
    @Override
    public void onResume() {
        super.onResume();

        cart.addCartListener(cartListener2);

    }

    private void init(){
        if(getActivity()==null)return;

        ((MainActivity)getActivity()).setCurrentScreenId(Constants.SCREEN_CART);
         dbMan=DBManager.getInstance(getActivity().getApplication());


        if(cart.getFirstOrder()!=null)
        outlet=dbMan.getOutletById(cart.getFirstOrder().getOutlet_id());
        else if(cart==null || cart.getOrders()==null){
                //forceChangetoOutletScreen();
            getActivity().finish();
        }
        if(outlet==null){
            getActivity().finish();
        }


        addDeliveryTypesToGroup();
        Toolbox.changeActionBarTitle(getActivity(),"Cart",outlet.getName());
        ImageView img=(ImageView)((ActionBarActivity)getActivity()).getSupportActionBar().getCustomView().findViewById(R.id.imgBack);
        img.setVisibility(View.VISIBLE);
        View headerView=LayoutInflater.from(getActivity()).inflate(R.layout.cart_list_header,null);
         footerView=LayoutInflater.from(getActivity()).inflate(R.layout.cart_list_footer,null);
        txtCartTotalVal=(TextView)footerView.findViewById(R.id.txt_cart_total_val);
        TextView txtDeliveryTime=(TextView)footerView.findViewById(R.id.txt_delivery_time);
        txtCartTotalVal.setText(""+Toolbox.formatDecimal(cart.getTotal()));
        if(outlet.getDelivery_time()!=null)
        {
            txtDeliveryTime.setText("Delivery in "+Toolbox.formatDecimal(outlet.getDelivery_time())+" mins");
        }else{
            txtDeliveryTime.setText("Delivery in "+Toolbox.formatDecimal(30)+" mins");
        }
     if(adp==null) {
         adp = new CartListAdapter((MainActivity) getActivity(), outlet.getCompany_id(), outlet.getId());

     }
        if(lstContent.getHeaderViewsCount()==0)
            lstContent.addHeaderView(headerView);
        if(lstContent.getFooterViewsCount()==0)
            lstContent.addFooterView(footerView);

        lstContent.setAdapter(adp);
    }

    CartListener cartListener2=new CartListener() {
        @Override
        public void onCartChange() {
            txtCartTotalVal.setText(""+Toolbox.formatDecimal(cart.getTotal()));
      }
    };
    private void forceChangetoOutletScreen(){
        List<String> backStackEntryList = new ArrayList<String>();
        for (int i = 0; i < getActivity().getSupportFragmentManager().getBackStackEntryCount(); i++) {
            android.support.v4.app.FragmentManager.BackStackEntry entry = getActivity().getSupportFragmentManager().getBackStackEntryAt(i);
            backStackEntryList.add(entry.getName());
            // Toolbox.writeToLog("Backstack@"+i+ "= "+entry.getName());
        }
        if (backStackEntryList.indexOf("screen_id_" + Constants.SCREEN_OUTLETS) != -1) {
            getActivity().getSupportFragmentManager().popBackStackImmediate("screen_id_" + Constants.SCREEN_OUTLETS, 0);
        }

        Toolbox.changeScreen(getActivity(), Constants.SCREEN_THANKS, true, null);
    }
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        cart.removeCartListener(cartListener2);

    }

    @Override
    public int getScreenIndex() {
        if(cartReachedByProceed)
        {
            return TopAndBottomBarFragment.HIDDEN;
        }
        else
        {
            return TopAndBottomBarFragment.CART;
        }
    }

    @Override
    public void onCartClicked() {
        //do nothing
    }

    @Override
    public void onProceed() {
        Outlet outlet=dbMan.getOutletById(cart.getFirstOrder().getOutlet_id());

        if(outlet.getMin_order_amnt()!=null && cart.getTotal()<outlet.getMin_order_amnt() && cart.getDelivery_type().toLowerCase().contains("deliver")){

            String message = "Minimum order amount for "+outlet.getCompany_outlet_name()+" is "+Toolbox.formatDecimal(outlet.getMin_order_amnt())+"." +
                    "\n\nYour cart total is "+cart.getTotal()+".\nPlease add more items to the cart.";
            CustomDialog.createCustomDialog(getActivity(),message,"OK",null,null,true,null);

        }
        else{
            Bundle b = new Bundle();
            b.putLong(Constants.OUTLET_ID,cart.getFirstOrder().getOutlet_id());
            Toolbox.changeScreen(getActivity(),Constants.SCREEN_PERSONAL_DETAILS,true,b);
        }


    }

    private void addDeliveryTypesToGroup(){
        if(outlet!=null){

            deliveryTypes=outlet.getDelivery_type().split(",");
            int i=0;
            for(String s:deliveryTypes){
                if(((LinearLayout)rgOrderDeliveryType).getChildCount()<2)
                rgOrderDeliveryType.addView(createRadioButton(s, i), i++);

            }
            rgOrderDeliveryType.setOnCheckedChangeListener(deliveryTypeRadioListener);

            if(rgOrderDeliveryType.getChildCount()>0){
                rgOrderDeliveryType.getChildAt(0).performClick();
            }
//            llOrderDateTime.setOnCheckedChangeListener(deliveryTimeRadioListener);

            rgOrderDeliveryType.setVisibility(View.VISIBLE);

        }

    }
    private RadioButton createRadioButton(String text,int id) {

        RadioButton radioButton = (RadioButton)LayoutInflater.from(getActivity()).inflate(R.layout.order_details_radio_button,null);
//        RadioButton radioButton = (RadioButton)viewGroup.findViewById(R.id.rb);
        radioButton.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT, 1.0f));

        radioButton.setText(text);
        radioButton.setId(id);
        //radioButton.setButtonDrawable(getResources().getDrawable(R.drawable.box8_radio_selector));
        //radioButton.setBackground(getResources().getDrawable(R.drawable.box8_radio_selector));
        return radioButton;
    }

    private RadioGroup.OnCheckedChangeListener deliveryTypeRadioListener=new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {

                  RadioButton rb=(RadioButton)radioGroup.findViewById(checkedId);
                  String deliveryType=rb.getText().toString();
                  Cart.getInstance().setDelivery_type(deliveryType);

//            relDeliveryTime.setVisibility(View.VISIBLE);

        }
    };

    @Override
    public String getProceedButtonText() {
        return "Next";
    }


}
